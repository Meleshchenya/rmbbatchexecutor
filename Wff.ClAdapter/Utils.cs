﻿using NLog;
using NLog.Config;
using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Wff.ClAdapter
{
    internal static class Utils
    {
        private const string ConnectionFormat = "ModulePath={0},ConnectionString={1},Kind=FileStorage,PlatformPath={2}";
        private const string TempProcessCmdFormat = "/C (\"{0}\" {1}) && (del \"{0}\")";

        public static HandlerExecutor CreateExecutor(string applicationDirectory)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            LogManager.Configuration = new XmlLoggingConfiguration(config.FilePath);

            Logger logger = LogManager.GetLogger("HandlerExecutor");

            var adapterConfig = GetAdapterConfig();

            return new HandlerExecutor(logger, Path.Combine(applicationDirectory, adapterConfig.Executable));
        }

        public static HandlerExecutor CreateCmdExecutor()
        {
            Logger logger = LogManager.GetLogger("HandlerExecutor");

            return new HandlerExecutor(logger, "cmd.exe");
        }

        public static string CreateConnection(string applicationDirectory)
        {
            var adapterConfig = GetAdapterConfig();
            var envPath = Path.Combine(applicationDirectory, adapterConfig.EnvPath);
            var connectionString = Path.Combine(applicationDirectory, adapterConfig.ConnectionString);
            var platformsPath = Path.Combine(applicationDirectory, adapterConfig.PlatformsPath);
            
            return string.Format(ConnectionFormat, envPath, connectionString, platformsPath);
        }

        public static string GetOrThrow(string name)
        {
            string value = ConfigurationManager.AppSettings.Get(name);
            if (value == null)
                throw new ConfigurationErrorsException(name);

            return value;
        }

        public static void CopyAll(DirectoryInfo source, DirectoryInfo target)
        {
            Directory.CreateDirectory(target.FullName);

            foreach (FileInfo fi in source.GetFiles())
                fi.CopyTo(Path.Combine(target.FullName, fi.Name), true);


            foreach (DirectoryInfo diSourceSubDir in source.GetDirectories())
            {
                DirectoryInfo nextTargetSubDir = target.CreateSubdirectory(diSourceSubDir.Name);
                CopyAll(diSourceSubDir, nextTargetSubDir);
            }
        }

        public static void LaunchTempProcess(string executablePath, params string[] args)
        {
            string ticks = DateTime.Now.Ticks.ToString();
            var tempExecutablePath = Path.Combine(Path.GetTempPath(), ticks + ".exe");
            File.Copy(executablePath, tempExecutablePath);

            string cmd = string.Format(TempProcessCmdFormat, tempExecutablePath, string.Join(" ", args.Select(_ => $"\"{_}\"")));
            Process.Start("cmd.exe", cmd);
        }

        public static bool HasErrors(HandlerExecutor.ExecutionResult result)
        {
            return result.HasError || result.Output.Contains("encountered an error");
        }

        public static string GetRootDir() =>
            Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);

        private static AdapterConfig GetAdapterConfig()
        {
            var pluginLocation = BatchExecutorSettings.Default.PluginLocation;

            var configProvider = new ConfigProvider<AdapterConfig>();
            return configProvider.GetConfig(Path.Combine(pluginLocation, "AdapterConfig.json"));
        }
    }
}
