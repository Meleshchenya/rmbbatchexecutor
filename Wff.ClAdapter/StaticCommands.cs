﻿using System;
using System.IO;
using System.Threading;
using Wff.ClAdapter.CacheTools;

namespace Wff.ClAdapter
{
    public static class StaticCommands
    {
        private static readonly Lazy<HandlerExecutor> CmdExecutor = new Lazy<HandlerExecutor>(Utils.CreateCmdExecutor);

        #region Commands

        public static void RunConfiguration(string applicationDirectory, string configurationId, string date = null)
        {
            var executor = Utils.CreateExecutor(applicationDirectory);
            var connection = Utils.CreateConnection(applicationDirectory);

            var result = date == null
                ? ExecutorCommands.RunConfiguration(executor, connection, configurationId)
                : ExecutorCommands.RunConfiguration(executor, connection, configurationId, date);

            if (Utils.HasErrors(result))
                throw new Exception(result.Output);
        }

        public static void RunJob(string applicationDirectory, string jobId)
        {
            var executor = Utils.CreateExecutor(applicationDirectory);
            var connection = Utils.CreateConnection(applicationDirectory);

            var result = ExecutorCommands.RunJob(executor, connection, jobId);
            if (Utils.HasErrors(result))
                throw new Exception(result.Output);
        }

        public static void RunWorkItem(string applicationDirectory, string workItemId)
        {
            var executor = Utils.CreateExecutor(applicationDirectory);
            var connection = Utils.CreateConnection(applicationDirectory);

            var result = ExecutorCommands.RunWorkItem(executor, connection, workItemId);
            if (Utils.HasErrors(result))
                throw new Exception(result.Output);
        }

        public static void ExecutePnlHandler(string applicationDirectory, string pnlCalculationId, string handler)
        {
            var executor = Utils.CreateExecutor(applicationDirectory);
            var connection = Utils.CreateConnection(applicationDirectory);

            var result = ExecutorCommands.ExecutePnlHandler(executor, connection, pnlCalculationId, handler);
            if (Utils.HasErrors(result))
                throw new Exception(result.Output);
        }

        public static void RevertEnvironment(string applicationDirectory)
        {
            var executor = Utils.CreateExecutor(applicationDirectory);
            var connection = Utils.CreateConnection(applicationDirectory);

            var result = ExecutorCommands.RevertInitialState(executor, connection);
            if (Utils.HasErrors(result))
                throw new Exception(result.Output);
        }

        public static void ExtractUserStorage(string applicationDirectory)
        {
            var executor = Utils.CreateExecutor(applicationDirectory);
            var connection = Utils.CreateConnection(applicationDirectory);

            var result = ExecutorCommands.ExtractUserStorage(executor, connection);
            if (Utils.HasErrors(result))
                throw new Exception(result.Output);
        }

        public static void CopyFolder(string source, string destination)
        {
            DirectoryInfo diSource = new DirectoryInfo(source);
            DirectoryInfo diTarget = new DirectoryInfo(destination);
            Utils.CopyAll(diSource, diTarget);
        }

        public static void CopyFile(string sourceFileName, string destFileName)
        {
            File.Copy(sourceFileName, destFileName, true);
        }

        public static void DeleteFolder(string folder, bool throwIfNotFound = false)
        {
            const int numberOfRetries = 3;
            const int delayOnRetry = 1000;
            for (int i = 0; i < numberOfRetries; ++i)
            {
                try
                {
                    Directory.Delete(folder, true);
                }
                catch (DirectoryNotFoundException)
                {
                    if (throwIfNotFound)
                        throw;

                    return;
                }
                catch (IOException)
                {
                    if (i == numberOfRetries)
                        throw;

                    Thread.Sleep(delayOnRetry);
                }
            }
        }
        
        public static void MergeCache(string sourceCachePath, string destinationCachePath, string filterMask = null)
        {
            CacheMergeTool.MergeCaches(destinationCachePath, sourceCachePath, filterMask);
        }

        public static void Update(string updatePackageLocation)
        {
            string updaterPath = Utils.GetOrThrow(AdapterConfigKeys.UpdaterPath);
            string serviceName = Utils.GetOrThrow(AdapterConfigKeys.HostServiceName);
            string rootDir = Utils.GetRootDir();

            Utils.LaunchTempProcess(updaterPath, serviceName, rootDir, updatePackageLocation);            
        }

        public static void KillProcess(string processName)
        {
            var result = CmdExecutor.Value.Execute(new[] { $"/C Taskkill /IM {processName} /F" });
            if (result.HasError)
                throw new Exception(result.Output);
        }

        #endregion Commands
    }
}
