﻿using System.IO;
using System.Linq;

namespace Wff.ClAdapter.CacheTools
{
    public class DirectoryHelper
    {

        public static void CopyFolder(string source, string target, Filter filter)
        {
            DirectoryInfo diSource = new DirectoryInfo(source);
            DirectoryInfo diTarget = new DirectoryInfo(target);

            CopyFolder(diSource, diTarget, filter);
        }

        public static void CopyFolder(DirectoryInfo source, DirectoryInfo target, Filter filter)
        {
            Directory.CreateDirectory(target.FullName);

            foreach (FileInfo fi in source.GetFiles().Where(a => filter.Match(a.Name)))
                fi.CopyTo(Path.Combine(target.FullName, fi.Name), true);


            foreach (DirectoryInfo diSourceSubDir in source.GetDirectories())
            {
                DirectoryInfo nextTargetSubDir = target.CreateSubdirectory(diSourceSubDir.Name);
                CopyFolder(diSourceSubDir, nextTargetSubDir, filter);
            }
        }
    }
}
