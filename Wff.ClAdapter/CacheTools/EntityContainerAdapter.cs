﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using Cl.Analyst.Data;

namespace Wff.ClAdapter.CacheTools
{
    [DebuggerDisplay("{EntityName}")]
    public class EntityContainerAdapter : IEquatable<EntityContainerAdapter>
    {
        public const string BinDataExt = "clbindata";
        public const string BinKeyExt = "clbinkey";

        private readonly string _binDataFilePath;
        private readonly string _binKeyFilePath;

        private readonly Lazy<FileInfo> _binDataFileInfo;
        private readonly Lazy<FileInfo> _binKeyFileInfo;
        private readonly Lazy<DateTime> _lastModified;
        private readonly Lazy<byte[]> _binKeyFileHash;

        public string EntityName { get; }
        public long BinDataSize => _binDataFileInfo.Value.Length;
        public long BinKeySize => _binKeyFileInfo.Value.Length;
        public DateTime LastModified => _lastModified.Value;
        public byte[] BinKeyFileHash => _binKeyFileHash.Value;

        public EntityContainerAdapter(string binDataFilePath, string binKeyFilePath)
        {
            var directoryName = Path.GetDirectoryName(binDataFilePath);
            if (directoryName == null)
                throw new ArgumentException("path");

            EntityName = Path.GetFileNameWithoutExtension(binDataFilePath);
            _binKeyFileHash = new Lazy<byte[]>(() => GetFileHash(binKeyFilePath));
            
            _binDataFilePath = binDataFilePath;
            _binKeyFilePath = binKeyFilePath;

            _binDataFileInfo = new Lazy<FileInfo>(() => new FileInfo(_binDataFilePath));
            _binKeyFileInfo = new Lazy<FileInfo>(() => new FileInfo(_binKeyFilePath));
            
            _lastModified = new Lazy<DateTime>(() =>
            {
                DateTime dataModificationTime = _binDataFileInfo.Value.LastWriteTime;
                DateTime keyModificationTime = _binKeyFileInfo.Value.LastWriteTime;
                return dataModificationTime > keyModificationTime ? dataModificationTime : keyModificationTime;
            });
        }

        public static bool TryCreateNew(string datasetFolderPath, HashSet<string> existingFiles, string name, out EntityContainerAdapter container)
        {
            container = null;

            if (!existingFiles.Contains(name))
                return false;

            string keyFilePath = Path.Combine(datasetFolderPath, name + "." + BinKeyExt);
            string dataFilePath = Path.Combine(datasetFolderPath, name + "." + BinDataExt);
            container = new EntityContainerAdapter(dataFilePath, keyFilePath);
            return true;
        }

        public void CopyTo(string destinationPath)
        {
            var dataCopyPath = Path.Combine(destinationPath, EntityName + "." + BinDataExt);
            _binDataFileInfo.Value.CopyTo(dataCopyPath, true);
            
            var keyCopyPath = Path.Combine(destinationPath, EntityName + "." + BinKeyExt);
            _binKeyFileInfo.Value.CopyTo(keyCopyPath, true);
        }

        public void Merge(EntityContainerAdapter otherContainer)
        {
            var otherIndex = CreateIndex(otherContainer._binKeyFilePath);
            var otherEntries = ReadIndexEntries(otherIndex);

            var targetIndex = CreateIndex(_binKeyFilePath);
            var targetStream = File.Open(_binDataFilePath, FileMode.Open, FileAccess.ReadWrite);
            var otherStream = File.Open(otherContainer._binDataFilePath, FileMode.Open, FileAccess.Read);

            try
            {
                var targetEntryKeys = new HashSet<ClStrongHash>(ReadIndexEntries(targetIndex).Select(_ => _.Key));
                targetStream.Seek(0, SeekOrigin.End);

                using (targetIndex.AcquireWrite())
                {
                    foreach (var otherEntry in otherEntries)
                    {
                        ClStrongHash key = otherEntry.Key;
                        if (!targetEntryKeys.Contains(key))
                            CopyEntity(key, targetStream, targetIndex, otherStream, otherEntry.Value);
                        else
                            ResolveConflict(key, targetStream, targetIndex, otherStream, otherEntry.Value, otherContainer);
                    }
                }
            }
            finally
            {
                targetIndex.Dispose();
                targetStream.Dispose();
                otherIndex.Dispose();
                otherStream.Dispose();
            }
        }
        
        public bool Equals(EntityContainerAdapter other)
        {
            if (other != null && BinKeySize == other.BinKeySize && BinDataSize == other.BinDataSize)
                return BinKeyFileHash.SequenceEqual(other.BinKeyFileHash);

            return false;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (BinDataSize.GetHashCode() * 397) ^ BinKeySize.GetHashCode();
            }
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(this, obj)) return true;
            var other = obj as EntityContainerAdapter;
            if (other == null)
                return false;

            return Equals(other);
        }

        public override string ToString() =>
            _binDataFilePath;

        private static IEnumerable<KeyValuePair<ClStrongHash, IndexEntry>> ReadIndexEntries(Index index)
        {
            using (index.AcquireRead())
                return index.GetAllEntries();
        }

        private static Index CreateIndex(string path) =>
            new Index(path, 0, new ReloadState(), FileMode.Open, FileAccess.ReadWrite, stream => stream.Length, false);

        private void ResolveConflict(
            ClStrongHash entityHash,
            Stream targetStream,
            Index targetIndex,
            Stream otherStream,
            IndexEntry otherEntry,
            EntityContainerAdapter otherContainer)
        {
            KeyValuePair<ClStrongHash, IndexEntry> entry;
            if (!targetIndex.TryGet(entityHash, out entry) || entry.Value.Position < 0)
                return;

            if (otherEntry.Position < 0)
                targetIndex.DeleteEntry(entityHash, otherEntry.Position == IndexEntry.AntiEntity);
            else if (otherContainer.LastModified > LastModified)
                CopyEntity(entityHash, targetStream, targetIndex, otherStream, otherEntry);
        }

        private static void CopyEntity(
            ClStrongHash entityHash,
            Stream targetStream,
            Index targetIndex,
            Stream otherStream,
            IndexEntry otherEntry)
        {
            var bytes = new byte[otherEntry.Size];

            if (otherEntry.Position >= 0)
            {
                otherStream.Seek(otherEntry.Position, SeekOrigin.Begin);
                otherStream.Read(bytes, 0, otherEntry.Size);
            }

            targetIndex.AddEntry(entityHash, targetStream.Position, otherEntry.Size);
            targetStream.Write(bytes, 0, otherEntry.Size);
        }

        private static byte[] GetFileHash(string fileName)
        {
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(fileName))
                {
                    return md5.ComputeHash(stream);
                }
            }
        }
    }
}
