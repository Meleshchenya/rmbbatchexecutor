﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Wff.ClAdapter.CacheTools
{
    public class Filter
    {
        private readonly Func<string, bool> _match;

        public Filter(string mask)
        {
            if (string.IsNullOrEmpty(mask))
                _match = s => true;
            else
                _match = s => Regex.IsMatch(s, mask);
        }

        public bool Match(string shortFileName)
        {
            return _match(shortFileName);
        }
    }

    public class CacheMergeTool
    {
        private static readonly string[] SystemDatasets = { "#system#" };

        public static void MergeCaches(string destinationPath, string sourcePath, string filterMask = null)
        {
            var filter = new Filter(filterMask);

            if (!Directory.Exists(destinationPath) || Directory.GetFiles(destinationPath, "*", SearchOption.AllDirectories).Length == 0)
            {
                DirectoryHelper.CopyFolder(sourcePath, destinationPath, filter);
                return;
            }

            CopySystemDataset(destinationPath, sourcePath, new Filter(null));

            var sourceCache = new CacheAdapter(sourcePath);
            var destinationCache = new CacheAdapter(destinationPath);

            var existingDatasetNames = sourceCache.GetDatasetNames();

            foreach (var datasetName in existingDatasetNames)
            {
                if (sourceCache.TryGetDataset(datasetName, out var dataset))
                    destinationCache.AddDataset(dataset, filter);
            }
        }

        private static void CopySystemDataset(string destinationPath, string sourcePath, Filter filter)
        {
            foreach (var systemDataset in SystemDatasets)
            {
                string destinationDatasetPath = Path.Combine(destinationPath, systemDataset);
                if (!Directory.Exists(destinationPath))
                {
                    string sourceDatasetPath = Path.Combine(destinationPath, sourcePath);
                    if (Directory.Exists(sourceDatasetPath))
                    {
                        DirectoryHelper.CopyFolder(sourceDatasetPath, destinationDatasetPath, filter);
                        break;
                    }
                }
            }
        }
    }
}
