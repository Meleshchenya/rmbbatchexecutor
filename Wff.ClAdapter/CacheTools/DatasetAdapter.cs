﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace Wff.ClAdapter.CacheTools
{
    [DebuggerDisplay("{Name}")]
    public class DatasetAdapter
    {
        private readonly string _datasetPath;
        private readonly HashSet<string> _existingFiles; 

        public DatasetAdapter(string datasetPath, string datasetName)
        {
            _datasetPath = datasetPath;
            _existingFiles = new HashSet<string>(Directory.GetFiles(_datasetPath).Select(Path.GetFileNameWithoutExtension));
            Name = datasetName;
        }

        public string Name { get; }

        public void Merge(DatasetAdapter otherDataset, Filter filter)
        {
            var existingContainerNames =
                otherDataset.GetContainerNames(filter).Concat(GetContainerNames(filter)).Distinct();

            foreach (var containerName in existingContainerNames)
            {
                bool targetExists = TryGetEntityContainer(containerName, out var targetContainer);
                bool sourceExists = otherDataset.TryGetEntityContainer(containerName, out var sourceContainer);

                if (sourceExists && !targetExists)
                    sourceContainer.CopyTo(_datasetPath);
                else if (sourceExists && !sourceContainer.Equals(targetContainer))
                    targetContainer.Merge(sourceContainer);
            }
        }

        public void CopyTo(string destinationPath, Filter filter)
        {
            DirectoryHelper.CopyFolder(_datasetPath, destinationPath, filter);
        }

        public IList<string> GetContainerNames(Filter filter)
        {
            List<string> containerNames = Directory.GetFiles(_datasetPath)
                .Select(Path.GetFileNameWithoutExtension)
                .Where(filter.Match)
                .Distinct()
                .ToList();

            return containerNames;
        }

        public bool TryGetEntityContainer(string containerName, out EntityContainerAdapter container)
        {
            return EntityContainerAdapter.TryCreateNew(_datasetPath, _existingFiles, containerName, out container);
        } 
    }
}
