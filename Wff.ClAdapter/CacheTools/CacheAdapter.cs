﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace Wff.ClAdapter.CacheTools
{
    [DebuggerDisplay("{_mainPath}")]
    public class CacheAdapter
    {
        public const string MainCacheFolder = "Default\\bin";

        private readonly string _mainPath;

        public CacheAdapter(string path)
        {
            _mainPath = Path.Combine(path, MainCacheFolder);
        }

        public IList<string> GetDatasetNames()
        {
            return Directory.GetDirectories(_mainPath).Select(Path.GetFileName).ToList();
        }

        public bool TryGetDataset(string datasetName, out DatasetAdapter dataset)
        {
            dataset = null;
            string datasetDirectory = Path.Combine(_mainPath, datasetName);
            if (!Directory.Exists(datasetDirectory))
                return false;

            dataset = new DatasetAdapter(datasetDirectory, datasetName);
            return true;
        }

        public void AddDataset(DatasetAdapter dataset, Filter filter)
        {
            if (TryGetDataset(dataset.Name, out var existingDataset))
                existingDataset.Merge(dataset, filter);
            else
                dataset.CopyTo(Path.Combine(_mainPath, dataset.Name), filter);
        }
    }
}
