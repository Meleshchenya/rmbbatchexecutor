﻿using System;
using System.Collections.Generic;

namespace Wff.ClAdapter
{
    internal static class ExecutorCommands
    {
        public static HandlerExecutor.ExecutionResult RevertInitialState(HandlerExecutor consoleExecutor, string connection)
        {
            return consoleExecutor.Execute(new[]
            {
                "-Mode=ModifyEnvironment",
                "-Command=RevertToInitialState",
                string.Format(@"-Source=""{0}""", connection),
                "-SyncFileNames=false",
                "-ReplaceCodeOnDiskWithGenerated=false",
                "-Env=Default"
            });
        }

        public static HandlerExecutor.ExecutionResult ExtractUserStorage(HandlerExecutor consoleExecutor, string connection)
        {
            return consoleExecutor.Execute(new[]
            {
                "-Mode=ExecuteHandler",
                "-Handler=Extract",
                string.Format(@"-Source=""{0}""", connection),
                @"-Key=""<Cl.Analyst.ClXmlRepositoryKey><ID>UserRepo</ID></Cl.Analyst.ClXmlRepositoryKey>"""
            });
        }

        public static HandlerExecutor.ExecutionResult RunConfiguration(
            HandlerExecutor consoleExecutor,
            string connection,
            string configurationId)
        {
            return consoleExecutor.Execute(new[]
            {
                "-Mode=ExecuteHandler",
                "-Handler=Configure",
                string.Format(@"-Source=""{0}""", connection),
                string.Format(@"-Key=""<Cl.Analyst.Finance.ClConfigurationKey><ConfigurationID>{0}</ConfigurationID></Cl.Analyst.Finance.ClConfigurationKey>""", configurationId)
            });
        }

        public static HandlerExecutor.ExecutionResult RunConfiguration(
            HandlerExecutor consoleExecutor,
            string connection,
            string configurationId,
            string date)
        {
            return consoleExecutor.Execute(new[]
            {
                "-Mode=ExecuteHandler",
                "-Handler=ConfigureOnDate",
                $@"-Source=""{connection}""",
                $@"-Key=""<Cl.Analyst.Finance.ClConfigurationKey><ConfigurationID>{configurationId}</ConfigurationID></Cl.Analyst.Finance.ClConfigurationKey>""",
                $@"-ValuationDate=""{date}"""
            });
        }

        public static HandlerExecutor.ExecutionResult RunJob(
            HandlerExecutor consoleExecutor,
            string connection,
            string jobId)
        {
            return consoleExecutor.Execute(new[]
            {
                "-Mode=ExecuteHandler",
                "-Handler=QueueJob",
                string.Format(@"-Source=""{0}""", connection),
                string.Format(@"-Key=""<Cl.Analyst.ClJobKey><JobID>{0}</JobID></Cl.Analyst.ClJobKey>""", jobId)
            });
        }

        public static HandlerExecutor.ExecutionResult RunWorkItem(
            HandlerExecutor consoleExecutor,
            string connection,
            string workItemId)
        {
            return consoleExecutor.Execute(new[]
            {
                "-Mode=ExecuteHandler",
                "-Handler=DoWork",
                string.Format(@"-Source=""{0}""", connection),
                string.Format(@"-Key=""<Cl.Analyst.Finance.ClWorkItemKey><WorkItemID>{0}</WorkItemID></Cl.Analyst.Finance.ClWorkItemKey>""", workItemId)
            });
        }

        public static HandlerExecutor.ExecutionResult ExecutePnlHandler(
            HandlerExecutor consoleExecutor,
            string connection,
            string pnlCalculationId,
            string handler)
        {
            return consoleExecutor.Execute(new[]
            {
                "-Mode=ExecuteHandler",
                "-Handler=" + handler,
                string.Format(@"-Source=""{0}""", connection),
                string.Format(@"-Key=""<Rmb.Finance.Pnl.RmbPnlDistributedCalculationKey><PnlCalculation><Tree><ID>{0}</ID></Tree></PnlCalculation></Rmb.Finance.Pnl.RmbPnlDistributedCalculationKey>""", pnlCalculationId)
            });
        }

        public static HandlerExecutor.ExecutionResult ExecuteXhHandler(
            HandlerExecutor consoleExecutor,
            string connection,
            string handlerName,
            Dictionary<string, string> parameters)
        {
            var paramsStr = "";
            foreach (var parameter in parameters)
                paramsStr += $@" -{parameter.Key}=""{parameter.Value}""";

            return consoleExecutor.Execute(new[]
            {
                "-Mode=ExecuteHandler",
                "-Handler=" + handlerName,
                $@"-Source=""{connection}""",
                $@"-Type=""Rmb.Xh.RmbXhRunnerData"" {paramsStr}"
            });
        }
    }
}
