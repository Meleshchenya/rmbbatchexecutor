﻿using System;

namespace Wff.ClAdapter
{
    public class AdapterConfig
    {
        private string _executable;
        private string _envPath;
        private string _connectionString;
        private string _platformsPath;

        public string Executable
        {
            get
            {
                Validate(_executable, "Executable");
                return _executable;
            }
            set => _executable = value;
        }

        public string EnvPath
        {
            get
            {
                Validate(_envPath, "EnvPath");
                return _envPath;
            }
            set => _envPath = value;
        }

        public string ConnectionString
        {
            get
            {
                Validate(_connectionString, "ConnectionString");
                return _connectionString;
            }
            set => _connectionString = value;
        }

        public string PlatformsPath
        {
            get
            {
                Validate(_platformsPath, "PlatformsPath");
                return _platformsPath;
            }
            set => _platformsPath = value;
        }

        private static void Validate(string value, string name)
        {
            if (string.IsNullOrEmpty(value))
                throw new Exception($"'{name}' field is not defined.");
        }
    }
}
