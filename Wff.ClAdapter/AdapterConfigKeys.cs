﻿namespace Wff.ClAdapter
{
    static class AdapterConfigKeys
    {
        public const string UpdaterPath = "updaterPath";
        public const string HostServiceName = "HostServiceName";
    }
}
