﻿using System;
using System.Collections.Generic;

namespace Wff.ClAdapter
{
    public static class XhStaticCommands
    {
        #region Commands

        public static void CreateConfigurations(
            string applicationDirectory, string configurationName, string configurationsCount, string date = null, string trades = null)
        {
            var executor = Utils.CreateExecutor(applicationDirectory);
            var connection = Utils.CreateConnection(applicationDirectory);

            var parameters = new Dictionary<string, string>
            {
                { "ConfigurationName", configurationName },
                { "ConfigurationsCount", configurationsCount }
            };
            
            if (!string.IsNullOrEmpty(date))
                parameters.Add("Date", date);

            if (!string.IsNullOrEmpty(trades))
                parameters.Add("Trades", trades.Replace(';', ','));

            var result = ExecutorCommands.ExecuteXhHandler(executor, connection, "CreateConfigurations", parameters);
            if (Utils.HasErrors(result))
                throw new Exception(result.Output);
        }

        public static void SendMailReport(string applicationDirectory, string from, string to, string smtpHost)
        {
            var executor = Utils.CreateExecutor(applicationDirectory);
            var connection = Utils.CreateConnection(applicationDirectory);

            var parameters = new Dictionary<string, string>
            {
                { "From", from },
                { "To", to },
                { "SmtpHost", smtpHost }
            };

            var result = ExecutorCommands.ExecuteXhHandler(executor, connection, "SendMailReport", parameters);
            if (Utils.HasErrors(result))
                throw new Exception(result.Output);
        }

        #endregion Commands
    }
}
