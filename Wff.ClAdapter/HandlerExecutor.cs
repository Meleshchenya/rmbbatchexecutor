﻿using NLog;
using System.Diagnostics;
using System.Text;
using System.Threading;

namespace Wff.ClAdapter
{
    internal class HandlerExecutor
    {
        private readonly string _executor;
        private readonly Logger _logger;

        public HandlerExecutor(Logger logger, string executor)
        {
            _executor = executor;
            _logger = logger;
        }

        public ExecutionResult Execute(string[] args)
        {
            var output = new StringBuilder();
            var error = new StringBuilder();

            using (var process = new Process())
            {
                process.StartInfo.FileName = _executor;
                process.StartInfo.Arguments = string.Join(" ", args);
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.ErrorDialog = false;

                using (var outputWaitHandle = new AutoResetEvent(false))
                using (var errorWaitHandle = new AutoResetEvent(false))
                {
                    process.OutputDataReceived += (sender, e) =>
                    {
                        if (e.Data == null)
                        {
                            outputWaitHandle.Set();
                        }
                        else
                        {
                            output.AppendLine(e.Data);
                            _logger.Info(e.Data);
                        }
                    };
                    process.ErrorDataReceived += (sender, e) =>
                    {
                        if (e.Data == null)
                        {
                            errorWaitHandle.Set();
                        }
                        else
                        {
                            error.AppendLine(e.Data);
                            _logger.Error(e.Data);
                        }
                    };

                    process.Start();

                    process.BeginOutputReadLine();
                    process.BeginErrorReadLine();

                    process.WaitForExit();
                    outputWaitHandle.WaitOne();
                    errorWaitHandle.WaitOne();
                }
            }

            return new ExecutionResult(output.ToString(), error.ToString());
        }

        internal class ExecutionResult
        {
            public ExecutionResult(string standard, string error)
            {
                var output = new StringBuilder();

                if (!string.IsNullOrEmpty(error))
                {
                    output.AppendLine(error);
                    HasError = true;
                }

                if (!string.IsNullOrEmpty(standard))
                    output.AppendLine(standard);

                Output = output.ToString();
            }

            public string Output { get; private set; }
            public bool HasError { get; private set; }
        }
    }
}
