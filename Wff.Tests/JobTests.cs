﻿using System;
using System.IO;
using NUnit.Framework;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;

namespace Wff.Tests
{
    [TestFixture()]
    public class JobTests
    {
        private const string ExpectedValue = "expected_value" + nameof(JobTests);
        private const string OldExpectedValue = ExpectedValue + "_old";
        
        [Test]
        public void RunJobCommandWithOptionalParametersWithoutParameters()
        {
            var job = new Job(typeName: TestCommanContainer.TypeName, handler: "CommandWithOptionalArguments");
            TestCommanContainer.Field = String.Empty;
            JobResult res = job.Run(TestExecutionContext.Instance, CancellationToken.None);
            var resValue = BinarySerializer.Deserialize<string>(res.Body);
            
            Assert.IsNotNull(res);
            Assert.AreEqual(JobExecutionStatus.Completed, res.Status);
            Assert.IsTrue(res.HasBody);
            Assert.AreEqual("55", resValue);
        }
        
        [Test]
        public void RunJobCommandWithOptionalParametersWithParameter()
        {
            var job = new Job(typeName: TestCommanContainer.TypeName, handler: "CommandWithOptionalArguments", args: "44");
            TestCommanContainer.Field = String.Empty;
            JobResult res = job.Run(TestExecutionContext.Instance, CancellationToken.None);
            var resValue = BinarySerializer.Deserialize<string>(res.Body);
            
            Assert.IsNotNull(res);
            Assert.AreEqual(JobExecutionStatus.Completed, res.Status);
            Assert.IsTrue(res.HasBody);
            Assert.AreEqual("44", resValue);
        }
        
        [Test]
        public void RunJobCommandWithParameters()
        {
            var job = new Job(typeName: TestCommanContainer.TypeName, handler: "CommandWithArguments", args: "44,");
            TestCommanContainer.Field = String.Empty;
            JobResult res = job.Run(TestExecutionContext.Instance, CancellationToken.None);
            var resValue = BinarySerializer.Deserialize<string>(res.Body);
            
            Assert.IsNotNull(res);
            Assert.AreEqual(JobExecutionStatus.Completed, res.Status);
            Assert.IsTrue(res.HasBody);
            Assert.AreEqual("44", resValue);
        }
        
        [Test]
        public void RunJobWithEmptyResultTest()
        {
            var job = new Job(typeName: TestCommanContainer.TypeName, handler: "Execute", args: ExpectedValue);
            TestCommanContainer.Field = String.Empty;
            JobResult res = job.Run(TestExecutionContext.Instance, CancellationToken.None);

            Assert.IsNotNull(res);
            Assert.AreEqual(JobExecutionStatus.Completed, res.Status);
            Assert.IsFalse(res.HasBody);
            Assert.AreEqual(ExpectedValue, TestCommanContainer.Field);
        }

        [Test]
        public void RunJobWithNonEmptyResultSuccessfulTest()
        {
            TestCommanContainer.Field = OldExpectedValue;
            var job = new Job(typeName: TestCommanContainer.TypeName, handler: "ExecuteWithResult", args: ExpectedValue);
            JobResult res = job.Run(TestExecutionContext.Instance, CancellationToken.None);

            Assert.IsNotNull(res);
            Assert.AreEqual(JobExecutionStatus.Completed, res.Status);
            Assert.IsTrue(res.HasBody);

            string resBody = res.GetResult<string>();
            Assert.AreEqual(OldExpectedValue, resBody);
            Assert.AreEqual(ExpectedValue, TestCommanContainer.Field);
        }

        [Test]
        public void RunJobExpectedFailTest()
        {
            TestCommanContainer.Field = OldExpectedValue;
            var job = new Job(typeName: TestCommanContainer.TypeName, handler: "ExecuteWithFail");
            JobResult res = job.Run(TestExecutionContext.Instance, CancellationToken.None);

            Assert.IsNotNull(res);
            Assert.AreEqual(JobExecutionStatus.Failed, res.Status);
            Assert.IsInstanceOf(typeof(WffException), res.JobException);
        }

        [Test]
        public void SerializeDeserializeTest()
        {
            IJob job = new Job(typeName: TestCommanContainer.TypeName, handler: "Execute", args: ExpectedValue);

            using (var stream = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(stream, job);

                stream.Seek(0, SeekOrigin.Begin);
                object obj = formatter.Deserialize(stream);

                Assert.IsInstanceOf(typeof(IJob), obj);

                job = (IJob)obj;
                TestCommanContainer.Field = String.Empty;
                job.Run(TestExecutionContext.Instance, CancellationToken.None);

                Assert.AreEqual(ExpectedValue, TestCommanContainer.Field);
            }
        }
    }
}
