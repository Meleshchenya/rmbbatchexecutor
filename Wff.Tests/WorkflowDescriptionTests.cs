﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Wff.Xml;

namespace Wff.Tests
{
    [TestFixture]
    public class WorkflowDescriptionTests
    {
        #region Test Workflow

        private static string ComplexWorkflowXml = @"
<Workflow storage=""batch_storage"">
	<Executors>
		<add id = ""cronus"" host=""cronus"" />
		<add id = ""crius"" host=""crius"" />
	</Executors>	
	<WorkItems>	
		<run
            id = ""importTrades""
            type=""Wff.ClAdapter.StaticCommands, Wff.ClAdapter""
			handler=""ImportTrades""
			execution=""cronus"" />
		<run
            id = ""evaluate""
            type=""Wff.ClAdapter.StaticCommands, Wff.ClAdapter""
			handler=""EvaluateStatistic""
			execution=""cronus"" />
		<runAsync
            id = ""configure_cronus""
            type=""Wff.ClAdapter.StaticCommands, Wff.ClAdapter""
			handler=""Configure""
			execution=""cronus""
			args=""filter1""/>
		<runAsync
            id = ""configure_crius""
            type=""Wff.ClAdapter.StaticCommands, Wff.ClAdapter""
			handler=""Configure""
			execution=""cronus""
			args=""filter2"" />
		<barrier waitFor = ""configure_cronus,configure_crius"" />
        <run
            id=""calculate""
			type=""Wff.ClAdapter.StaticCommands, Wff.ClAdapter""
			handler=""Calculate""
			execution=""cronus,crius"" />
		<run
            id = ""aggregate""
            type=""Wff.ClAdapter.StaticCommands, Wff.ClAdapter""
			handler=""Aggregate""
			execution=""cronus"" />
		<run
            id = ""reportStatus""
            type=""Wff.ClAdapter.StaticCommands, Wff.ClAdapter""
			handler=""ReportStatus""
			execution=""cronus"" />
	</WorkItems>
</Workflow>";

        private static string SimpleWorkflowXml = @"
<Workflow storage=""batch_storage"">
	<Executors>
		<add id = ""cronus"" host=""cronus"" />
	</Executors>	
	<WorkItems>	
		<run
            id = ""importTrades""
            type=""Wff.ClAdapter.StaticCommands, Wff.ClAdapter""
			handler=""ImportTrades""
			execution=""cronus""
            attribute1=""value1""
            attribute2=""value2"" />		
	</WorkItems>
</Workflow>";
	    
	    private static string WorkflowWithVariablesSection = @"
<Workflow storage=""batch_storage"">
	<Executors>
		<add id = ""cronus"" host=""cronus"" />
	</Executors>
    <Variables>
        <add key=""Id"" value=""importTrades"" />
        <add key=""HeadNode"" value=""cronus"" />
        <add key=""Type"" value=""Wff.ClAdapter.StaticCommands, Wff.ClAdapter"" />
        <add key=""Handler"" value=""ImportTrades"" />
        <add key=""Arguments"" value=""D:\folder"" />
    </Variables>
	<WorkItems>	
		<run
            id = ""{Id}""
            type=""{Type}""
			handler=""{Handler}""
			execution=""{HeadNode}""
            args=""{Arguments}"" />		
	</WorkItems>
</Workflow>";
	    
	    private static string WorkflowWithArguments = @"
<Workflow storage=""batch_storage"">
	<Executors>
		<add id = ""cronus"" host=""cronus"" />
	</Executors>
	<WorkItems>	
		<run
            id = ""@missing""
            type=""Wff.ClAdapter.StaticCommands, Wff.ClAdapter""
			handler=""ImportTrades""
			execution=""@headNode""
            args=""D:\folder,@asOfDate,test@test.com,@trades"" />		
	</WorkItems>
</Workflow>";

        #endregion Test Workflow

        [Test]
        public void LoadTest()
        {
            using (var tempFile = TempFile.Create())
            {
                tempFile.WriteText(ComplexWorkflowXml);

                var workflow = new WorkflowDescription();
                workflow.Load(tempFile.FilePath);

                Assert.IsNotNull(workflow.Executors);
                Assert.AreEqual(2, workflow.Executors.Count);
                Assert.IsNotNull(workflow.WorkItems);
                Assert.AreEqual(8, workflow.WorkItems.Count);
            }
        }

        [Test]
        public void LoadAttributesTest()
        {
            using (var tempFile = TempFile.Create())
            {
                tempFile.WriteText(SimpleWorkflowXml);

                var workflow = new WorkflowDescription();
                workflow.Load(tempFile.FilePath);

                Assert.IsNotNull(workflow.Executors);
                Assert.AreEqual(1, workflow.Executors.Count);
                Assert.IsNotNull(workflow.WorkItems);
                Assert.AreEqual(1, workflow.WorkItems.Count);
                Assert.IsNotNull(workflow.WorkItems[0].ExecutionAttributes);
                Assert.AreEqual(3, workflow.WorkItems[0].ExecutionAttributes.Count);
            }
        }

        [Test]
        public void BuildWorkItemTest()
        {
            using (var tempFile = TempFile.Create())
            {
                tempFile.WriteText(ComplexWorkflowXml);

                var workflowDescription = new WorkflowDescription();
                workflowDescription.Load(tempFile.FilePath);

                var workflow = new HostDependentWorkflow(workflowDescription);
                List<IWorkItem<HostInfo>> workItems = workflow.ToList();

                Assert.AreEqual(8, workItems.Count);
                Assert.IsInstanceOf(typeof(WorkItem), workItems[0]);
                Assert.IsInstanceOf(typeof(WorkItem), workItems[1]);
                Assert.IsInstanceOf(typeof(AsyncWorkItem), workItems[2]);
                Assert.IsInstanceOf(typeof(AsyncWorkItem), workItems[3]);
                Assert.IsInstanceOf(typeof(Barrier), workItems[4]);
                Assert.IsInstanceOf(typeof(WorkItem), workItems[5]);
                Assert.IsInstanceOf(typeof(WorkItem), workItems[6]);
                Assert.IsInstanceOf(typeof(WorkItem), workItems[7]);
            }
        }
	    
	    [Test]
	    public void LoadWorkflowWithVariablesTest()
	    {
		    using (var tempFile = TempFile.Create())
		    {
			    tempFile.WriteText(WorkflowWithVariablesSection);

			    var workflow = new WorkflowDescription();
			    workflow.Load(tempFile.FilePath);

			    Assert.IsNotNull(workflow.Variables);
			    Assert.AreEqual(5, workflow.Variables.Count);
			    Assert.AreEqual(workflow.Variables.First(a => a.Key == "Id").Value, workflow.WorkItems[0].Id);
			    Assert.AreEqual(workflow.Variables.First(a => a.Key == "Type").Value, workflow.WorkItems[0].TargetType);
			    Assert.AreEqual(workflow.Variables.First(a => a.Key == "Handler").Value, workflow.WorkItems[0].Handler);
			    Assert.AreEqual(workflow.Variables.First(a => a.Key == "Arguments").Value, workflow.WorkItems[0].Args);
			    Assert.AreEqual(workflow.Variables.First(a => a.Key == "HeadNode").Value, workflow.WorkItems[0].ExecutionAttributes["execution"]);
		    }
	    }
	    
	    [Test]
	    public void LoadWorkflowWithArgumentsTest()
	    {
		    using (var tempFile = TempFile.Create())
		    {
			    tempFile.WriteText(WorkflowWithArguments);

			    var arguments = new Dictionary<string, string>
			    {
				    {"HEADNODE", "cronus" },
				    {"asOfDate", "2017-11-24"},
					{"Trades", "t1;t2;t3"}
				};
			    
			    var workflow = new WorkflowDescription();
			    workflow.Load(tempFile.FilePath, arguments);

			    Assert.AreEqual("", workflow.WorkItems[0].Id);
			    Assert.AreEqual("cronus", workflow.WorkItems[0].ExecutionAttributes["execution"]);
			    Assert.AreEqual("D:\\folder,2017-11-24,test@test.com,t1;t2;t3", workflow.WorkItems[0].Args);
		    }
	    }
    }
}
