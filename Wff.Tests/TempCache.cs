﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Cl.Analyst.Data;
using Wff.ClAdapter.CacheTools;

namespace Wff.Tests
{
    class TempCache : IDisposable
    {
        public static TempCache Create()
        {
            var folderName = Guid.NewGuid().ToString();

            var path = Path.Combine(Path.GetTempPath(), folderName);
            Directory.CreateDirectory(path);

            return new TempCache { FolderPath = path };
        }

        public void CreateDataset(string name)
        {
            string datasetPath = FormatDatasetPath(name);
            Directory.CreateDirectory(datasetPath);
        }

        public bool DatasetExists(string name)
        {
            string datasetPath = FormatDatasetPath(name);
            return Directory.Exists(datasetPath);
        }

        public TempEntityContainer CreateEntityContainer(string name, string dataset)
        {
            if (!DatasetExists(dataset))
                CreateDataset(dataset);

            string datasetPath = FormatDatasetPath(dataset);
            return new TempEntityContainer(datasetPath, name);
        }

        public bool TryGetEntityContainer(string name, string dataset, out TempEntityContainer entityContainer)
        {
            entityContainer = null;
            var datasetPath = FormatDatasetPath(dataset);

            if (!TempEntityContainer.Exists(datasetPath, name))
                return false;

            entityContainer = new TempEntityContainer(datasetPath, name);
            return true;
        }

        private string FormatDatasetPath(string dataset)
        {
            string datasetPath = Path.Combine(FolderPath, CacheAdapter.MainCacheFolder, dataset);
            return datasetPath;
        }

        public bool EntityContainerExists(string name, string dataset)
        {
            string datasetPath = FormatDatasetPath(dataset);
            return TempEntityContainer.Exists(datasetPath, name);
        }

        public string FolderPath { get; private set; }

        public void Dispose()
        {
            if (Directory.Exists(FolderPath))
                Directory.Delete(FolderPath, true);
        }

        public class TempEntityContainer : IEquatable<TempEntityContainer>
        {
            private readonly string _keyFilePath;
            private readonly string _dataFilePath;

            public static bool Exists(string datasetPath, string name)
            {
                string keyFilePath = Path.Combine(datasetPath, name + "." + EntityContainerAdapter.BinKeyExt);
                string dataFilePath = Path.Combine(datasetPath, name + "." + EntityContainerAdapter.BinDataExt);

                return File.Exists(keyFilePath) && File.Exists(dataFilePath);
            }

            public TempEntityContainer(string datasetPath, string name)
            {
                _keyFilePath = Path.Combine(datasetPath, name + "." + EntityContainerAdapter.BinKeyExt);
                _dataFilePath = Path.Combine(datasetPath, name + "." + EntityContainerAdapter.BinDataExt);
            }

            public void AddEntry(ClStrongHash hashKey, string data)
            {
                using (var index = new Index(_keyFilePath, 0, new ReloadState(), FileMode.OpenOrCreate, FileAccess.ReadWrite, stream => stream.Length, false))
                using (var dataStream = File.Open(_dataFilePath, FileMode.OpenOrCreate, FileAccess.ReadWrite))
                {
                    byte[] bytes = Encoding.Default.GetBytes(data);
                    dataStream.Seek(0, SeekOrigin.End);

                    using (index.AcquireWrite())
                        index.AddEntry(hashKey, dataStream.Position, bytes.Length);

                    dataStream.Write(bytes, 0, bytes.Length);
                }
            }

            public bool TryGetEntry(ClStrongHash hashKey, out string data)
            {
                data = null;
                using (var index = new Index(_keyFilePath, 0, new ReloadState(), FileMode.Open, FileAccess.Read, stream => stream.Length, false))
                using (var dataStream = File.Open(_dataFilePath, FileMode.Open, FileAccess.Read))
                { 
                    KeyValuePair<ClStrongHash, IndexEntry> entry;
                    using (index.AcquireRead())
                    {
                        if (!index.TryGet(hashKey, out entry))
                            return false;
                    }

                    var bytes = new byte[entry.Value.Size];
                    dataStream.Seek(entry.Value.Position, SeekOrigin.Begin);
                    dataStream.Read(bytes, 0, entry.Value.Size);

                    data = Encoding.Default.GetString(bytes);
                    return true;
                }
            }

            #region IEquatable Implementation 

            public bool Equals(TempEntityContainer other)
            {
                if (other == null
                    || !Path.GetFileName(_keyFilePath).Equals(Path.GetFileName(other._keyFilePath)) 
                    || !Path.GetFileName(_keyFilePath).Equals(Path.GetFileName(other._keyFilePath)))
                    return false;

                var dataFileInfo = new FileInfo(_dataFilePath);
                var keyFileInfo = new FileInfo(_keyFilePath);

                var otherDataFileInfo = new FileInfo(other._dataFilePath);
                var otherKeyFileInfo = new FileInfo(other._keyFilePath);

                return dataFileInfo.Length == otherDataFileInfo.Length && keyFileInfo.Length == otherKeyFileInfo.Length;
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != this.GetType()) return false;
                return Equals((TempEntityContainer) obj);
            }

            public override int GetHashCode()
            {
                return _keyFilePath.GetHashCode() ^ _dataFilePath.GetHashCode();
            }

            public static bool operator ==(TempEntityContainer left, TempEntityContainer right)
            {
                return Equals(left, right);
            }

            public static bool operator !=(TempEntityContainer left, TempEntityContainer right)
            {
                return !Equals(left, right);
            }

            #endregion IEquatable Implementation
        }
    }
}
