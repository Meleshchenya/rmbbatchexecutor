﻿using System.Collections.Generic;
using NUnit.Framework;
using Wff.Xml;

namespace Wff.Tests
{
    [TestFixture]
    public class WorkflowTests
    {
        private const string expectedValue = "expected_value"+nameof(WorkflowTests);

        [Test]
        public void RunWorkflowTest()
        {
            var job = new Job(TestCommanContainer.TypeName, "Execute", expectedValue);
            var localHost = new HostInfo("localhost");
            var executor = new AtlExecutor(TestExecutionContext.Instance);
            var scheduler = new RemoteScheduler();
            scheduler.AddExecutor(localHost, executor);

            var workItems = new List<IWorkItem<HostInfo>>
            {
                new WorkItem("Test", job, new List<HostInfo>{ localHost })
            };

            var workflow = new HostDependentWorkflow(workItems);

            TestCommanContainer.Field = string.Empty;
            scheduler.ExecuteWorkflow(workflow);
            Assert.AreEqual(expectedValue, TestCommanContainer.Field);
        }
    }
}
