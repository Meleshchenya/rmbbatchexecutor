﻿using System;
using System.IO;

namespace Wff.Tests
{
    class TempFile : IDisposable
    {
        public static TempFile Create()
        {
            var path = Path.GetTempFileName();
            File.Create(path).Close();

            return new TempFile { FilePath = path };
        }

        public string FilePath { get; private set; }

        public void WriteText(string text)
        {
            File.WriteAllText(FilePath, text);
        }

        public void Dispose()
        {
            if (File.Exists(FilePath))
                File.Delete(FilePath);
        }
    }
}
