﻿using System;

namespace Wff.Tests
{
    public class TestCommanContainer
    {
        public static readonly string TypeName = typeof(TestCommanContainer).FullName;

        public static string Field;

        public static void Execute(string value)
        {
            Field = value;
        }

        public static string ExecuteWithResult(string value)
        {
            string currentValue = Field;
            Field = value;

            return currentValue;
        }

        public static void ExecuteWithFail()
        {
            throw new WffException(String.Empty);
        }
        
        public static string CommandWithOptionalArguments(string argument = "55")
        {
            return argument;
        }
        
        public static string CommandWithArguments(string arg1)
        {
            return arg1;
        }
        
        public static string CommandWithArguments(string arg1, string arg2)
        {
            return arg2;
        }

        public static string CommandWithParams(params string[] pars)
        {
            return "1";
        }
    }
}
