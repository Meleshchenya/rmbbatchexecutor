﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Framing;

namespace Wff.Tests
{
    [TestFixture]
    public class RemoteExecutorClientTests
    {
        private const string Host = "host";
        private const string ExceptionMessage = "Exception";

        private readonly RemoteExecutorClient _remoteExecutorClient;
        
        public RemoteExecutorClientTests()
        {
            IConnection connection = Mock.Of<IConnection>(c => c.CreateModel() == new ModelStub());
            _remoteExecutorClient = new RemoteExecutorClient(connection, Host);
        }

        [Test]
        public void RunCompleted()
        {
            var job = new JobStub(JobExecutionStatus.Completed);
            Task<JobResult> task = _remoteExecutorClient.RunAsync(job, CancellationToken.None);

            task.Wait();
            Assert.IsTrue(task.IsCompleted);
        }
        
        [Test]
        public void RunCanceled()
        {
            var job = new JobStub(JobExecutionStatus.Canceled);
            var source = new CancellationTokenSource();
            var task = _remoteExecutorClient.RunAsync(job, source.Token);

            source.Cancel();

            Assert.IsTrue(task.IsCanceled);
        }

        [Test]
        public void RunFailed()
        {
            var job = new JobStub(JobExecutionStatus.Failed);
            Task<JobResult> task = _remoteExecutorClient.RunAsync(job, CancellationToken.None);

            try
            {
                task.Wait();
            }
            catch (AggregateException exc)
            {
                Assert.AreEqual(ExceptionMessage, exc.InnerException?.InnerException?.Message);
            }

            Assert.IsTrue(task.IsFaulted);
        }

        #region Stubs

        class ModelStub : IModel
        {
            private IBasicConsumer _consumer;

            public string BasicConsume(string queue, bool autoAck, string consumerTag, bool noLocal, bool exclusive, IDictionary<string, object> arguments,
                IBasicConsumer consumer)
            {
                _consumer = consumer;
                return String.Empty;
            }

            public void BasicPublish(string exchange, string routingKey, bool mandatory, IBasicProperties basicProperties, byte[] body)
            {
                if (body == null || body.Length == 0)
                {
                    byte[] canceledBody = BinarySerializer.Serialize(JobResult.Canceled());
                    _consumer.HandleBasicDeliver(String.Empty, 0, true, String.Empty, String.Empty, basicProperties, canceledBody);
                }
                else
                {
                    JobStub job = BinarySerializer.Deserialize<JobStub>(body);

                    Task.Factory.StartNew(() =>
                    {
                        switch (job.Status)
                        {
                            case JobExecutionStatus.Completed:
                                byte[] completedbody = BinarySerializer.Serialize(JobResult.Completed());
                                _consumer.HandleBasicDeliver(String.Empty, 0, true, String.Empty, String.Empty, basicProperties, completedbody);
                                break;

                            case JobExecutionStatus.Failed:
                                byte[] failedBody = BinarySerializer.Serialize(JobResult.Failed(new Exception(ExceptionMessage)));
                                _consumer.HandleBasicDeliver(String.Empty, 0, true, String.Empty, String.Empty, basicProperties, failedBody);
                                break;

                            case JobExecutionStatus.Canceled:
                                while (true) { }
                        }
                    });
                }
            }

            #region Empty Implementation

            public void Dispose()
            {
                throw new NotImplementedException();
            }

            public void Abort()
            {
                throw new NotImplementedException();
            }

            public void Abort(ushort replyCode, string replyText)
            {
                throw new NotImplementedException();
            }

            public void BasicAck(ulong deliveryTag, bool multiple)
            {
                throw new NotImplementedException();
            }

            public void BasicCancel(string consumerTag)
            {
                throw new NotImplementedException();
            }

            public BasicGetResult BasicGet(string queue, bool autoAck)
            {
                throw new NotImplementedException();
            }

            public void BasicNack(ulong deliveryTag, bool multiple, bool requeue)
            {
                throw new NotImplementedException();
            }

            public void BasicQos(uint prefetchSize, ushort prefetchCount, bool global)
            {
                throw new NotImplementedException();
            }

            public void BasicRecover(bool requeue)
            {
                throw new NotImplementedException();
            }

            public void BasicRecoverAsync(bool requeue)
            {
                throw new NotImplementedException();
            }

            public void BasicReject(ulong deliveryTag, bool requeue)
            {
                throw new NotImplementedException();
            }

            public void Close()
            {
                throw new NotImplementedException();
            }

            public void Close(ushort replyCode, string replyText)
            {
                throw new NotImplementedException();
            }

            public void ConfirmSelect()
            {
                throw new NotImplementedException();
            }

            public IBasicProperties CreateBasicProperties()
            {
                return new BasicProperties();
            }

            public void ExchangeBind(string destination, string source, string routingKey, IDictionary<string, object> arguments)
            {
                throw new NotImplementedException();
            }

            public void ExchangeBindNoWait(string destination, string source, string routingKey, IDictionary<string, object> arguments)
            {
                throw new NotImplementedException();
            }

            public void ExchangeDeclare(string exchange, string type, bool durable, bool autoDelete, IDictionary<string, object> arguments)
            {
                throw new NotImplementedException();
            }

            public void ExchangeDeclareNoWait(string exchange, string type, bool durable, bool autoDelete, IDictionary<string, object> arguments)
            {
                throw new NotImplementedException();
            }

            public void ExchangeDeclarePassive(string exchange)
            {
                throw new NotImplementedException();
            }

            public void ExchangeDelete(string exchange, bool ifUnused)
            {
                throw new NotImplementedException();
            }

            public void ExchangeDeleteNoWait(string exchange, bool ifUnused)
            {
                throw new NotImplementedException();
            }

            public void ExchangeUnbind(string destination, string source, string routingKey, IDictionary<string, object> arguments)
            {
                throw new NotImplementedException();
            }

            public void ExchangeUnbindNoWait(string destination, string source, string routingKey, IDictionary<string, object> arguments)
            {
                throw new NotImplementedException();
            }

            public void QueueBind(string queue, string exchange, string routingKey, IDictionary<string, object> arguments)
            {
                throw new NotImplementedException();
            }

            public void QueueBindNoWait(string queue, string exchange, string routingKey, IDictionary<string, object> arguments)
            {
                throw new NotImplementedException();
            }

            public QueueDeclareOk QueueDeclare(string queue, bool durable, bool exclusive, bool autoDelete, IDictionary<string, object> arguments)
            {
                return new QueueDeclareOk("Name", 1, 1);
            }

            public void QueueDeclareNoWait(string queue, bool durable, bool exclusive, bool autoDelete, IDictionary<string, object> arguments)
            {
                throw new NotImplementedException();
            }

            public QueueDeclareOk QueueDeclarePassive(string queue)
            {
                throw new NotImplementedException();
            }

            public uint MessageCount(string queue)
            {
                throw new NotImplementedException();
            }

            public uint ConsumerCount(string queue)
            {
                throw new NotImplementedException();
            }

            public uint QueueDelete(string queue, bool ifUnused, bool ifEmpty)
            {
                throw new NotImplementedException();
            }

            public void QueueDeleteNoWait(string queue, bool ifUnused, bool ifEmpty)
            {
                throw new NotImplementedException();
            }

            public uint QueuePurge(string queue)
            {
                throw new NotImplementedException();
            }

            public void QueueUnbind(string queue, string exchange, string routingKey, IDictionary<string, object> arguments)
            {
                throw new NotImplementedException();
            }

            public void TxCommit()
            {
                throw new NotImplementedException();
            }

            public void TxRollback()
            {
                throw new NotImplementedException();
            }

            public void TxSelect()
            {
                throw new NotImplementedException();
            }

            public bool WaitForConfirms()
            {
                throw new NotImplementedException();
            }

            public bool WaitForConfirms(TimeSpan timeout)
            {
                throw new NotImplementedException();
            }

            public bool WaitForConfirms(TimeSpan timeout, out bool timedOut)
            {
                throw new NotImplementedException();
            }

            public void WaitForConfirmsOrDie()
            {
                throw new NotImplementedException();
            }

            public void WaitForConfirmsOrDie(TimeSpan timeout)
            {
                throw new NotImplementedException();
            }

            public int ChannelNumber { get; }
            public ShutdownEventArgs CloseReason { get; }
            public IBasicConsumer DefaultConsumer { get; set; }
            public bool IsClosed { get; }
            public bool IsOpen { get; }
            public ulong NextPublishSeqNo { get; }
            public TimeSpan ContinuationTimeout { get; set; }
            public event EventHandler<BasicAckEventArgs> BasicAcks;
            public event EventHandler<BasicNackEventArgs> BasicNacks;
            public event EventHandler<EventArgs> BasicRecoverOk;
            public event EventHandler<BasicReturnEventArgs> BasicReturn;
            public event EventHandler<CallbackExceptionEventArgs> CallbackException;
            public event EventHandler<FlowControlEventArgs> FlowControl;
            public event EventHandler<ShutdownEventArgs> ModelShutdown;

            #endregion Empty Implementation
        }

        [Serializable]
        class JobStub : IJob
        {
            public JobStub(JobExecutionStatus status)
            {
                Status = status;
            }

            public JobExecutionStatus Status { get; }

            public JobResult Run(IExecutionContext context, CancellationToken cancellationToken)
            {
                throw new NotImplementedException();
            }
        }

        #endregion Stubs
    }

}
