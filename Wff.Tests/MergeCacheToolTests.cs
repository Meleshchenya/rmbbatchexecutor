﻿using Cl.Analyst.Data;
using NUnit.Framework;
using Wff.ClAdapter.CacheTools;

namespace Wff.Tests
{
    [TestFixture]
    public class MergeCacheToolTests
    {
        [Test]
        public void Merge_IntoEmptyCache_Test()
        {
            using (var sourceCache = TempCache.Create())
            using (var destinationCache = TempCache.Create())
            {
                var tradeContainer = sourceCache.CreateEntityContainer("Cl.Analyst.Finance.ClTrade", "Common");
                tradeContainer.AddEntry(ClStrongHash.Create("Trade"), "Trade");
                
                CacheMergeTool.MergeCaches(destinationCache.FolderPath, sourceCache.FolderPath);

                Assert.True(destinationCache.DatasetExists("Common"));

                TempCache.TempEntityContainer sourceContainer;
                sourceCache.TryGetEntityContainer("Cl.Analyst.Finance.ClTrade", "Common", out sourceContainer);

                TempCache.TempEntityContainer destinationContainer;
                Assert.True(destinationCache.TryGetEntityContainer("Cl.Analyst.Finance.ClTrade", "Common", out destinationContainer));

                Assert.AreEqual(sourceContainer, destinationContainer);

                string data;
                Assert.True(destinationContainer.TryGetEntry(ClStrongHash.Create("Trade"), out data));
                Assert.AreEqual("Trade", data);
            }
        }

        [Test]
        public void Merge_SameDataset_DifferentContainers_Test()
        {
            using (var sourceCache = TempCache.Create())
            using (var destinationCache = TempCache.Create())
            {
                var tradeContainer = sourceCache.CreateEntityContainer("Cl.Analyst.Finance.ClTrade", "Common");
                tradeContainer.AddEntry(ClStrongHash.Create("Trade"), "Trade");

                var cptyContainer = sourceCache.CreateEntityContainer("Cl.Analyst.Finance.ClCpty", "Common");
                cptyContainer.AddEntry(ClStrongHash.Create("Cpty"), "Cpty");

                CacheMergeTool.MergeCaches(destinationCache.FolderPath, sourceCache.FolderPath);

                Assert.True(destinationCache.DatasetExists("Common"));
                Assert.True(destinationCache.EntityContainerExists("Cl.Analyst.Finance.ClCpty", "Common"));
                Assert.True(destinationCache.EntityContainerExists("Cl.Analyst.Finance.ClTrade", "Common"));

                TempCache.TempEntityContainer sourceContainer;
                sourceCache.TryGetEntityContainer("Cl.Analyst.Finance.ClTrade", "Common", out sourceContainer);

                TempCache.TempEntityContainer destinationContainer;
                Assert.True(destinationCache.TryGetEntityContainer("Cl.Analyst.Finance.ClTrade", "Common", out destinationContainer));

                Assert.AreEqual(sourceContainer, destinationContainer);

                string data;
                Assert.True(destinationContainer.TryGetEntry(ClStrongHash.Create("Trade"), out data));
                Assert.AreEqual("Trade", data);
                
                Assert.True(destinationCache.TryGetEntityContainer("Cl.Analyst.Finance.ClCpty", "Common", out destinationContainer));
                Assert.True(destinationContainer.TryGetEntry(ClStrongHash.Create("Cpty"), out data));
                Assert.AreEqual("Cpty", data);
            }
        }
        
        [Test]
        public void Merge_SameDataset_SameContainer_Test()
        {
            using (var sourceCache = TempCache.Create())
            using (var destinationCache = TempCache.Create())
            {
                var sourceContainer = sourceCache.CreateEntityContainer("Cl.Analyst.Finance.ClTrade", "Common");
                sourceContainer.AddEntry(ClStrongHash.Create("Trade1"), "Trade1");

                var destinationContainer = destinationCache.CreateEntityContainer("Cl.Analyst.Finance.ClTrade", "Common");
                destinationContainer.AddEntry(ClStrongHash.Create("Trade22"), "Trade22");

                CacheMergeTool.MergeCaches(destinationCache.FolderPath, sourceCache.FolderPath);
                
                Assert.True(destinationCache.TryGetEntityContainer("Cl.Analyst.Finance.ClTrade", "Common", out destinationContainer));

                string data;
                Assert.True(destinationContainer.TryGetEntry(ClStrongHash.Create("Trade1"), out data));
                Assert.AreEqual("Trade1", data);

                Assert.True(destinationContainer.TryGetEntry(ClStrongHash.Create("Trade22"), out data));
                Assert.AreEqual("Trade22", data);
            }
        }

        [Test]
        public void Merge_SameDataset_SameContainer_WithFilter_Test()
        {
            using (var sourceCache = TempCache.Create())
            using (var destinationCache = TempCache.Create())
            {
                var sourceCptyContainer = sourceCache.CreateEntityContainer("Cl.Analyst.Finance.ClCpty", "Common");
                sourceCptyContainer.AddEntry(ClStrongHash.Create("Cpty1"), "Cpty1");
                var sourceCurveContainer = sourceCache.CreateEntityContainer("Cl.Analyst.Finance.ClCurve", "Common");
                sourceCurveContainer.AddEntry(ClStrongHash.Create("Curve1"), "Curve1");
                var sourceTradeContainer = sourceCache.CreateEntityContainer("Cl.Analyst.Finance.ClTrade", "Common");
                sourceTradeContainer.AddEntry(ClStrongHash.Create("Trade1"), "Trade1");
                
                var destinationCurveContainer = destinationCache.CreateEntityContainer("Cl.Analyst.Finance.ClCurve", "Common");
                destinationCurveContainer.AddEntry(ClStrongHash.Create("Curve22"), "Curve22");
                var destinationTradeContainer = destinationCache.CreateEntityContainer("Cl.Analyst.Finance.ClTrade", "Common");
                destinationTradeContainer.AddEntry(ClStrongHash.Create("Trade22"), "Trade22");

                CacheMergeTool.MergeCaches(destinationCache.FolderPath, sourceCache.FolderPath, "ClTrade");

                Assert.True(destinationCache.TryGetEntityContainer("Cl.Analyst.Finance.ClTrade", "Common", out destinationTradeContainer));
                Assert.True(destinationCache.TryGetEntityContainer("Cl.Analyst.Finance.ClCurve", "Common", out destinationCurveContainer));

                TempCache.TempEntityContainer destinationCptyContainer;
                Assert.False(destinationCache.TryGetEntityContainer("Cl.Analyst.Finance.ClCpty", "Common", out destinationCptyContainer));

                string data;
                Assert.True(destinationTradeContainer.TryGetEntry(ClStrongHash.Create("Trade1"), out data));
                Assert.AreEqual("Trade1", data);

                Assert.True(destinationTradeContainer.TryGetEntry(ClStrongHash.Create("Trade22"), out data));
                Assert.AreEqual("Trade22", data);

                Assert.True(destinationCurveContainer.TryGetEntry(ClStrongHash.Create("Curve22"), out data));
                Assert.AreEqual("Curve22", data);

                Assert.False(destinationCurveContainer.TryGetEntry(ClStrongHash.Create("Curve1"), out data));
            }
        }
    }
}
