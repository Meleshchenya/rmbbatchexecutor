﻿using System;
using System.Diagnostics;

namespace Wff.Updater
{
    class ServiceStopper : IDisposable
    {
        private readonly string _serviceName;

        private ServiceStopper(string serviceName)
        {
            _serviceName = serviceName;
        }

        public static IDisposable SuspendService(string serviceName)
        {
            var stopper = new ServiceStopper(serviceName);
            stopper.StopService();

            return stopper;
        }

        public void Dispose() => 
            StartService();

        private void StopService() =>
            RunNet("stop", _serviceName);

        private void StartService() =>
            RunNet("start", _serviceName);

        private static void RunNet(string command, string serviceName)
        {
            string args = string.Format("{0} \"{1}\"", command, serviceName);
            string fileName = "net";
            RunExternalProcess(fileName, args);
        }

        private static void RunExternalProcess(string fileName, string args)
        {
            using (var process = new Process())
            {
                process.StartInfo.FileName = fileName;
                process.StartInfo.Arguments = args;
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.ErrorDialog = false;

                process.OutputDataReceived += DataReceived;
                process.ErrorDataReceived += DataReceived;

                process.Start();

                process.BeginOutputReadLine();
                process.BeginErrorReadLine();

                process.WaitForExit();
            }
        }


        private static void DataReceived(object obj, DataReceivedEventArgs e) =>
            Console.WriteLine(e.Data);
    }
}
