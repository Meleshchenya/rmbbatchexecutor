﻿using System;
using System.IO;

namespace Wff.Updater
{
    class Program
    {
        static int Main(string[] args)
        {
            if (args.Length != 3)
            {
                Console.Error.WriteLine("Wrong args. Usage: Wff.Updater.exe service_name updatable_location update_package_location");
                return -1;
            }

            string serviceName = args[0];
            string updatableLocation = args[1];
            string updatePackageLocation = args[2];

            try
            {
                using (ServiceStopper.SuspendService(serviceName))
                {
                    if (!Directory.Exists(updatePackageLocation))
                    {
                        Console.Error.WriteLine("update package does not exist");
                        return -1;
                    }

                    Directory.Delete(updatableLocation, true);
                    Directory.CreateDirectory(updatableLocation);

                    DirectoryInfo diSource = new DirectoryInfo(updatePackageLocation);
                    DirectoryInfo diTarget = new DirectoryInfo(updatableLocation);
                    CopyAll(diSource, diTarget);
                }
            }
            catch(Exception exc)
            {
                Console.Error.WriteLine(exc.Message);
                return -1;
            }

            return 0;
        }

        private static void CopyAll(DirectoryInfo source, DirectoryInfo target)
        {
            Directory.CreateDirectory(target.FullName);

            foreach (FileInfo fi in source.GetFiles())
                fi.CopyTo(Path.Combine(target.FullName, fi.Name), true);


            foreach (DirectoryInfo diSourceSubDir in source.GetDirectories())
            {
                DirectoryInfo nextTargetSubDir = target.CreateSubdirectory(diSourceSubDir.Name);
                CopyAll(diSourceSubDir, nextTargetSubDir);
            }
        }
    }
}
