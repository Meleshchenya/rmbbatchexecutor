﻿using System;
using Cl.Service;
using System.Net;
using System.Collections.Generic;

namespace Wff.Slave
{
    public class SlaveService : IClService
    {
        private readonly NLog.Logger _logger = NLogManager.Logger;
        private readonly Stack<IDisposable> _disposable = new Stack<IDisposable>();
        private RemoteExecutor _executor;

        public void Start()
        {
            var pluginLocation = PathResolver.GetPath(BatchExecutorSettings.Default.PluginLocation);
            var executionContext = new DefaultExecutionContext(pluginLocation);
            var atlExecutor = new AtlExecutor(executionContext);

            string hostName = Dns.GetHostName();
            var host = new HostInfo(hostName);

            _executor = new RemoteExecutor(atlExecutor, hostName);
            _disposable.Push(_executor);

            var commandBus = new CommandBus();
            _disposable.Push(commandBus);

            var ack = new SubscriptionBinder<HostInfo>(p => SendACK(commandBus, host), s => null);
            _disposable.Push(commandBus.Subscribe(ControlCommand.Ping, ack));

            var add = new SubscriptionBinder<HostInfo>(p => SendAdd(commandBus, host), s => null);
            _disposable.Push(commandBus.Subscribe(ControlCommand.NotifyMasterStarted, add));

            _executor.ExecutorStarted += () =>
            {
                SendAdd(commandBus, host);
                _logger.Info("Executor started.");
            };

            _executor.ExecutorStopped += () =>
            {
                SendRemove(commandBus, host);
                _logger.Info("Executor stopped.");
            };

            _executor.Start();

            _logger.Info("Service initialized.");
        }

        public void Stop()
        {
            _executor?.Stop();

            while (_disposable.Count != 0)
                _disposable.Pop()?.Dispose();

            _logger.Info("Service stopped.");
        }
        public Logger Logger { get; set; }

        private static void SendACK(ICommandBus commandBus, HostInfo host) => ControlCommand.ACK.Publish(commandBus, host);
        private static void SendAdd(ICommandBus commandBus, HostInfo host) => ControlCommand.AddHost.Publish(commandBus, host);
        private static void SendRemove(ICommandBus commandBus, HostInfo host) => ControlCommand.RemoveHost.Publish(commandBus, host);
    }
}
