﻿using System;
using System.Collections.Generic;

namespace Wff.Master.CommandShell
{
    public class ArgumentsParser
    {
        public Dictionary<string, string> Parse(string[] args)
        {
            var parsedArgs = new Dictionary<string, string>();

            try
            {
                foreach (var arg in args)
                {
                    var split = arg.Split(new[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                    parsedArgs.Add(split[0], split[1]);
                }
            }
            catch (Exception)
            {
                throw new Exception("Error parsing arguments.");
            }

            return parsedArgs;
        }
    }
}