﻿using System;
using System.Globalization;
using System.Linq;
using System.Reflection;

namespace Wff.Master.CommandShell
{
    internal class InstanceCommand : BaseCommand
    {
        private readonly object _instance;

        public InstanceCommand(Object instance, MethodInfo methodInfo, CommandDescriptionAttribute descriptionAttribute) 
            : base(methodInfo, descriptionAttribute)
        {
            _instance = instance;
        }

        public override void Execute(string[] arguments)
        {
            var args = GetArguments(TargetMethod, arguments);
            TargetMethod.Invoke(_instance, BindingFlags.Default, CommandArgsBinder, args, CultureInfo.CurrentCulture);
        }
    }
}
