﻿using System;
using System.Diagnostics;
using System.Threading;
using static Wff.Master.CommandShell.Kernel;

namespace Wff.Master.CommandShell
{
    public class ExitTrigger : IDisposable
    {
        private readonly ManualResetEvent _exitEvent = new ManualResetEvent(false);

        [CommandDescription(CommandName = "exit", LongHelp = "Finish work.", Usage = "exit")]
        public void ForceExit()
        {
            ExitForced?.Invoke();

            _exitEvent.Set();
        }

        public void WaitForExit() =>
            _exitEvent.WaitOne();

        public event Action ExitForced;

        public void Dispose()
        {
            _exitEvent?.Dispose();
        }
    }
}
