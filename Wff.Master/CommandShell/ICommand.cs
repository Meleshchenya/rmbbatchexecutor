﻿using System;

namespace Wff.Master.CommandShell
{
    public interface ICommand : IEquatable<ICommand>
    {
        /// <summary>
        /// Returns the command name.
        /// </summary>
        /// <value>Name of the command.</value>
        string CommandName
        {
            get;
        }

        /// <summary>
        /// Returns a brief help message for the command.
        /// </summary>
        /// <value>The help message.</value>
        string Usage
        {
            get;
        }
        /// <summary>
        /// Returns a more detailed help message for the command.
        /// </summary>
        /// <value>The help message.</value>
        string LongHelp
        {
            get;
        }
        /// <summary>
        /// Executes the command.
        /// </summary>
        /// <param name="args">Arguments to pass to the command.</param>
        void Execute(string[] args);
    }
}
