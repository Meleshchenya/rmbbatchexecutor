﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace Wff.Master.CommandShell
{
    internal class CommandContainer : IEnumerable<ICommand>
    {
        #region Private Fields

        private readonly Dictionary<string, ICommand> _commandSet = new Dictionary<string, ICommand>();

        #endregion Private Fields

        #region Public Methods

        public void AddCommandsFromType(Type type)
        {
            foreach (MethodInfo mi in type.GetMethods(BindingFlags.Static|BindingFlags.Public))
            {
                object[] attributes = mi.GetCustomAttributes(typeof(CommandDescriptionAttribute), false);

                foreach (object o in attributes)
                {
                    CommandDescriptionAttribute commandDescriptionAttribute = o as CommandDescriptionAttribute;
                    if (commandDescriptionAttribute != null)
                    {
                        BaseCommand cmd = new BaseCommand(mi, commandDescriptionAttribute);
                        _commandSet.Add(cmd.CommandName, cmd);
                    }
                }
            }
        }

        public void AddCommandsFromInstance(object instance)
        {
            Type type = instance.GetType();
            foreach (MethodInfo mi in type.GetMethods(BindingFlags.Instance|BindingFlags.Public))
            {
                object[] attributes = mi.GetCustomAttributes(typeof(CommandDescriptionAttribute), false);

                foreach (object o in attributes)
                {
                    CommandDescriptionAttribute commandDescriptionAttribute = o as CommandDescriptionAttribute;
                    if (commandDescriptionAttribute != null)
                    {
                        InstanceCommand cmd = new InstanceCommand(instance, mi, commandDescriptionAttribute);
                        _commandSet.Add(cmd.CommandName, cmd);
                    }
                }
            }
        }

        public bool TryGetValue(string commandName, out ICommand command)
        {
            return _commandSet.TryGetValue(commandName, out command);
        }

        public IEnumerator<ICommand> GetEnumerator()
        {
            return _commandSet.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion Public Methods
    }
}
