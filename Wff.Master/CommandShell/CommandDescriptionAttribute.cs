﻿using System;

namespace Wff.Master.CommandShell
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
    public sealed class CommandDescriptionAttribute : Attribute
    {
        private string _longHelp;

        public string CommandName { get; set; }

        public string Usage { get; set; }

        public string LongHelp
        {
            get
            {
                return _longHelp ?? "usage: \n" + Usage;
            }
            set
            {
                _longHelp = value;
            }
        }
    }
}
