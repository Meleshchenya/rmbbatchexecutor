﻿using System;
using System.Globalization;
using System.Linq;
using System.Reflection;

namespace Wff.Master.CommandShell
{
    internal class BaseCommand : ICommand
    {
        protected readonly MethodInfo TargetMethod;
        private readonly CommandDescriptionAttribute _cmdDescr;
        protected static ShellCommandBinder CommandArgsBinder = new ShellCommandBinder();

        public BaseCommand(MethodInfo methodInfo, CommandDescriptionAttribute descriptionAttribute)
        {
            TargetMethod = methodInfo;
            _cmdDescr = descriptionAttribute;
        }

        public string CommandName
        {
            get
            {
                return _cmdDescr.CommandName;
            }
        }

        public virtual void Execute(string[] arguments)
        {
            var args = GetArguments(TargetMethod, arguments);
            TargetMethod.Invoke(null, BindingFlags.Default, CommandArgsBinder, args, CultureInfo.CurrentCulture);
        }

        public string Usage
        {
            get
            {
                return _cmdDescr.Usage;
            }
        }

        public string LongHelp
        {
            get { return String.IsNullOrEmpty(_cmdDescr.LongHelp) ? _cmdDescr.Usage : _cmdDescr.LongHelp; }
        }

        bool IEquatable<ICommand>.Equals(ICommand other)
        {
            return Equals(other);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj is BaseCommand && Equals((BaseCommand) obj);
        }

        private bool Equals(BaseCommand other)
        {
            return Equals(TargetMethod, other.TargetMethod) && Equals(_cmdDescr, other._cmdDescr);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((TargetMethod != null ? TargetMethod.GetHashCode() : 0) * 397) ^ (_cmdDescr != null ? _cmdDescr.GetHashCode() : 0);
            }
        }
        
        protected static object[] GetArguments(MethodInfo mi, string[] arguments)
        {
            bool hasParams = false;
            
            var parameters = mi.GetParameters();
            if (parameters.Length > 0)
                hasParams = parameters[parameters.Length - 1].GetCustomAttributes(typeof(ParamArrayAttribute), false).Length > 0;

            if (hasParams)
            {
                int paramsIndex = parameters.Length - 1;

                object[] args = new object[parameters.Length];
                for (int i = 0; i < paramsIndex; i++)
                    args[i] = arguments[i];

                Type paramsType = parameters[paramsIndex].ParameterType.GetElementType();
                Array paramsArray = Array.CreateInstance(paramsType, arguments.Length - paramsIndex);
                for (int i = 0; i < paramsArray.Length; i++)
                    paramsArray.SetValue(arguments[i + paramsIndex], i);

                args[paramsIndex] = paramsArray;

                return args;
            }

            return arguments.Cast<object>().ToArray();
        }
    }
}