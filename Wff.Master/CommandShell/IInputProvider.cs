﻿using System;
using System.Collections.Generic;

namespace Wff.Master.CommandShell
{
    public interface IInputProvider
    {
        bool Pull(out string[] input);
    }

    class ManualInputProvider : IInputProvider
    {
        public bool Pull(out string[] input)
        {
            string str = null;

            Console.Write(":> ");
            while (string.IsNullOrEmpty(str))
                str = Console.ReadLine();

            input = str.Split();

            return true;
        }
    }
}
