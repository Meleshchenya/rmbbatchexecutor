﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Wff.Master.CommandShell
{
    public class Shell
    {
        #region Private Fields

        private readonly CommandContainer _commandContainer;
        private readonly IInputProvider _inputProvider;
        private volatile bool _finish;

        #endregion Private Fields

        #region Constructors

        public Shell(IInputProvider inputprovider, IEnumerable<object> owningInstances)
            : this(inputprovider, new Type[0], owningInstances)
        { }

        public Shell(IEnumerable<Type> owningTypes, IEnumerable<object> owningInstances)
            : this(new ManualInputProvider(), owningTypes, owningInstances)
        { }
        
        public Shell(IInputProvider inputprovider, IEnumerable<Type> owningTypes, IEnumerable<object> owningInstances)
        {
            _inputProvider = inputprovider;
            _commandContainer = new CommandContainer();

            foreach (var owningType in owningTypes)
                _commandContainer.AddCommandsFromType(owningType);

            foreach (var owningInstance in owningInstances)
                _commandContainer.AddCommandsFromInstance(owningInstance);
        }

        #endregion Constructors

        #region Public Methods/Properties

        public void Start()
        {
            _finish = false;

            string[] input;
            while (!_finish && _inputProvider.Pull(out input))
            {
                string commandName = input[0];
                string[] args = input.Skip(1).ToArray();

                ICommand command;
                if (_commandContainer.TryGetValue(commandName, out command))
                {
                    try
                    {
                        command.Execute(args);
                    }
                    catch (Exception exc)
                    {
                        Console.WriteLine("Command '{0}' error:", commandName);
                        if (exc.InnerException != null)
                            Console.WriteLine(exc.InnerException.Message);
                        else
                            Console.WriteLine(exc.Message);
                    }
                }
                else
                    Console.WriteLine("Unknown command '{0}'.", commandName);
            }
        }

        public void Stop()
        {
            _finish = true;
        }

        public List<ICommand> AvailableCommands
        {
            get { return _commandContainer.ToList(); }
        }

        #endregion Public Methods/Properties
    }
}
