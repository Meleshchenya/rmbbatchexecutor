﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Wff.Master.CommandShell
{
    class QueueInputProvider : IInputProvider
    {
        private readonly Queue<string[]> _inputs;

        public QueueInputProvider(IEnumerable<string[]> commands)
        {
            _inputs = new Queue<string[]>(commands);
        }

        public QueueInputProvider(IEnumerable<string> commands)
        {
            _inputs = new Queue<string[]>(commands.Select(c => c.Split()));
        }

        public QueueInputProvider()
        {
            _inputs = new Queue<string[]>();
        }

        public bool Pull(out string[] input)
        {
            input = null;
            if (_inputs.Count == 0)
                return false;

            input = _inputs.Dequeue();

            Console.WriteLine(String.Join(" ", input));

            return true;
        }
    }
}
