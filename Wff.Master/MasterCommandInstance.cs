﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using Wff.Master.CommandShell;
using Wff.Xml;

namespace Wff.Master
{
    public class MasterCommandInstance : IDisposable
    {
        private readonly ExitTrigger _exitTrigger;
        private readonly CommandBus _commandBus;
        private readonly string _hostName;

        public MasterCommandInstance(ExitTrigger exitTrigger)
        {
            _exitTrigger = exitTrigger;
            _hostName = Dns.GetHostName();

            _commandBus = new CommandBus();
        }

        #region Shell Command

        [CommandDescription(CommandName = "ping", LongHelp = "Ping specified slaves.", Usage = "ping host-list")]
        public void Ping(string hostString)
        {
            string[] hosts = hostString.Split(',');

            if (hosts == null || hosts.Length == 0)
                throw new ArgumentException();

            var pendingHosts = new HashSet<string>(hosts, StringComparer.OrdinalIgnoreCase);
            var resetEvent = new AutoResetEvent(false);

            Action<HostInfo> ackReceived = h =>
            {
                if (pendingHosts.Remove(h.Host))
                {
                    Console.WriteLine($"ACK received from {h.Host}");
                    if (pendingHosts.Count == 0)
                        resetEvent.Set();
                }
            };

            var subscriptionBinder = new SubscriptionBinder<HostInfo>(ackReceived, _ => _.InitiatorHost);
            using (_commandBus.Subscribe(ControlCommand.ACK, subscriptionBinder))
            {
                ControlCommand.Ping.Publish(_commandBus, _hostName);
                if (!resetEvent.WaitOne(10000, false))
                {
                    foreach (var host in pendingHosts)
                        Console.WriteLine($"{host} is unreachable.");
                    return;
                }

                Console.WriteLine("Successful");
            }
        }

        [CommandDescription(CommandName = "run", LongHelp = "Run workflow from the specified file.", Usage = "run file-path [arguments (arg:value)]")]
        public void Run(string fileName, params string[] args)
        {
            var scheduler = new RemoteScheduler();
            var disposable = new ConcurrentStack<IDisposable>();

            var arguments = new ArgumentsParser().Parse(args);
            var workflowDescription = new WorkflowDescription();
            workflowDescription.Load(fileName, arguments);

            Action<HostInfo> connect = h => ConnectToRemoteExecutor(h, scheduler, disposable);
            var add = new SubscriptionBinder<HostInfo>(connect, s => s.InitiatorHost);
            disposable.Push(_commandBus.Subscribe(ControlCommand.AddHost, add));

            var remove = new SubscriptionBinder<HostInfo>(scheduler.RemoveExecutor, s => s.InitiatorHost);
            disposable.Push(_commandBus.Subscribe(ControlCommand.RemoveHost, remove));

            ControlCommand.NotifyMasterStarted.Publish(_commandBus, _hostName);
            
            if (!PingExecutors(workflowDescription.Executors.Select(_ => _.Host)))
            {
                Console.WriteLine(
                    "Workflow cannot be executed because not all executors are available. Use 'ping' to get online executors.");
            }
            else
            {
                var workflow = new HostDependentWorkflow(workflowDescription);
                try
                {
                    _exitTrigger.ExitForced += scheduler.InterruptExecution;

                    if (scheduler.ExecuteWorkflow(workflow))
                        Console.WriteLine("Done.");
                }
                finally
                {
                    _exitTrigger.ExitForced -= scheduler.InterruptExecution;
                }
            }

            IDisposable obj;
            while (disposable.TryPop(out obj))
                obj?.Dispose();
        }

        private void ConnectToRemoteExecutor(HostInfo h, RemoteScheduler scheduler, ConcurrentStack<IDisposable> disposable)
        {
            var executor = new RemoteExecutorClient(h.Host);
            disposable.Push(executor);
            scheduler.AddExecutor(h, executor);
        }

        #endregion Shell Commands

        public void Dispose()
        {
            _commandBus?.Dispose();
        }

        private bool PingExecutors(IEnumerable<string> executorHosts)
        {
            var pendingHosts = new HashSet<string>(executorHosts, StringComparer.OrdinalIgnoreCase);
            var resetEvent = new ManualResetEvent(false);

            Action<HostInfo> ackReceived = h =>
            {
                if (pendingHosts.Remove(h.Host) && pendingHosts.Count == 0)
                    resetEvent.Set();
            };

            var subscriptionBinder = new SubscriptionBinder<HostInfo>(ackReceived, _ => _.InitiatorHost);
            IDisposable unsubscriber = _commandBus.Subscribe(ControlCommand.ACK, subscriptionBinder);
            try
            {
                ControlCommand.Ping.Publish(_commandBus, _hostName);
                var pingExecutors = resetEvent.WaitOne(10000, false);
                return pingExecutors;
            }
            finally
            {
                unsubscriber.Dispose();
                resetEvent.Dispose();
            }
        }
    }
}
