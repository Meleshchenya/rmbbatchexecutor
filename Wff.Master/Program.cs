﻿using System;
using System.Threading.Tasks;
using Wff.Master.CommandShell;

namespace Wff.Master
{
    public class Program
    {
        private static Shell _shell;
        private static event Action ConsoleCtrlRaised;

        static void Main(string[] args)
        {
            Kernel.SetConsoleCtrlHandler(ConsoleCtrlCheck, true);

            if (args != null && args.Length > 0)
                RunFromArgs(args);
            else
                Run();
        }

        private static void RunFromArgs(string[] args)
        {
            var exitTrigger = new ExitTrigger();
            var instance = new MasterCommandInstance(exitTrigger);
            try
            {
                ConsoleCtrlRaised += exitTrigger.ForceExit;
                _shell = new Shell(new QueueInputProvider(new[] {args}),  new object [] {exitTrigger, instance});
                _shell.Start();
            }
            finally
            {
                ConsoleCtrlRaised -= exitTrigger.ForceExit;
                instance.Dispose();
                exitTrigger.Dispose();
            }
        }

        private static void Run()
        {
            var exitTrigger = new ExitTrigger();
            var instance = new MasterCommandInstance(exitTrigger);
            try
            {
                ConsoleCtrlRaised += exitTrigger.ForceExit;
                _shell = new Shell(new [] {typeof(Program)}, new object[] { exitTrigger, instance });

                Task.Factory.StartNew(_shell.Start);

                exitTrigger.WaitForExit();
                _shell.Stop();
            }
            finally
            {
                ConsoleCtrlRaised -= exitTrigger.ForceExit;
                instance.Dispose();
                exitTrigger.Dispose();
            }
        }

        [CommandDescription(CommandName = "help", LongHelp = "Print all command help.", Usage = "help")]
        public static void Help()
        {
            if (_shell == null)
                return;

            foreach (var command in _shell.AvailableCommands)
            {
                Console.WriteLine("{0,-15}{1}", command.CommandName, command.LongHelp);
                Console.WriteLine("{0,-15}{1} {2}", "", "Usage:", command.Usage);
            }
        }

        private static bool ConsoleCtrlCheck(Kernel.CtrlTypes ctrlType)
        {
            if (ConsoleCtrlRaised == null)
                return false;

            ConsoleCtrlRaised();
            return true;
        }
    }
}
