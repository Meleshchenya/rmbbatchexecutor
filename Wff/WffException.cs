﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Wff
{
    [Serializable]
    public class WffException : Exception
    {
        protected WffException(SerializationInfo info, StreamingContext context) : base(info, context)
        { }

        public WffException(string message) : base(message)
        { }

        public WffException(string message, Exception inner) : base(message, inner)
        { }
    }
}
