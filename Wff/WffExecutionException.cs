﻿using System;
using System.Runtime.Serialization;

namespace Wff
{
    [Serializable]
    public class WffExecutionException : WffException
    {
        public WffExecutionException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public WffExecutionException(string message) : base(message)
        {
        }

        public WffExecutionException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}
