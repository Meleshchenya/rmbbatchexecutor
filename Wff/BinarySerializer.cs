﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Wff
{
    public static class BinarySerializer
    {
        public static byte[] Serialize(object obj)
        {
            using (var stream = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(stream, obj);

                return stream.ToArray();
            }
        }

        public static TObject Deserialize<TObject>(byte[] bytes)
        {
            using (var stream = new MemoryStream(bytes))
            {
                var formatter = new BinaryFormatter();
                return (TObject)formatter.Deserialize(stream);
            }
        }
    }
}
