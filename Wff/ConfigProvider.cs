﻿using System;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Wff
{
    public class ConfigProvider<T>
    {
        public T GetConfig(string fileName)
        {
            try
            {
                var settings = new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                };

                var path = PathResolver.GetPath(fileName);
                var text = File.ReadAllText(path);

                return JsonConvert.DeserializeObject<T>(text, settings);
            }
            catch (FileNotFoundException e)
            {
                throw new Exception($"File not found: {e.FileName}.");
            }
        }
    }
}
