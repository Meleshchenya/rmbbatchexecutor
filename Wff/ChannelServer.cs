﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;

namespace Wff
{
    public abstract class ChannelServer : IDisposable
    {
        private readonly IModel _channel;
        private readonly IConnection _connection;
        private readonly EventingBasicConsumer _consumer;
        private bool _isStarted;

        protected ChannelServer(string queueName)
        {
            var configProvider = new ConfigProvider<RabbitMQConfig>();
            var config = configProvider.GetConfig("config.json");

            var factory = new ConnectionFactory
            {
                HostName = config.Host,
                Port = config.Port,
                AutomaticRecoveryEnabled = true,
                ContinuationTimeout = TimeSpan.FromSeconds(config.ContinuationTimeout),
                UserName = config.UserName,
                Password = config.Password
            };

            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.QueueDeclare(queueName, false, false, true, null);

            _consumer = new EventingBasicConsumer(_channel);
            _channel.BasicConsume(queueName, true, _consumer);
        }

        protected ChannelServer(IConnection connection, string queueName)
        {
            _connection = connection;
            _channel = _connection.CreateModel();
            _channel.QueueDeclare(queueName, false, false, true, null);

            _consumer = new EventingBasicConsumer(_channel);
            _channel.BasicConsume(queueName, true, _consumer);
        }
        
        public virtual void Start()
        {
            _consumer.Received += MessageReceived;
            _isStarted = true;
        }

        public virtual void Stop()
        {
            _consumer.Received -= MessageReceived;
            _isStarted = false;
        }

        public virtual void Dispose()
        {
            if (_isStarted)
                Stop();

            if (_channel != null)
            {
                _channel.Close();
                _channel.Dispose();
            }

            if (_connection != null)
            {
                _connection.Close();
                _connection.Dispose();
            }
        }

        protected abstract void MessageReceived(object sender, BasicDeliverEventArgs args);

        protected IModel Channel => _channel;
        protected IConnection Connection => _connection;
        protected EventingBasicConsumer Consumer => _consumer;
    }
}
