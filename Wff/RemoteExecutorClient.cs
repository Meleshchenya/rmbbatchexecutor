﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Wff
{
    public class RemoteExecutorClient : IJobExecutor, IDisposable
    {
        private readonly string _executorHost;
        private readonly IConnection _connection;
        private readonly IModel _channel;
        private readonly string _replyQueueName;
        private readonly EventingBasicConsumer _consumer;
        private readonly string _executorQueueName;

        public RemoteExecutorClient(string executorHost)
        {
            _executorHost = executorHost;
            _executorQueueName =  string.Format(Constants.ChannelQueueFormat, executorHost);

            var configProvider = new ConfigProvider<RabbitMQConfig>();
            var config = configProvider.GetConfig("config.json");

            var factory = new ConnectionFactory
            {
                HostName = config.Host,
                Port = config.Port,
                AutomaticRecoveryEnabled = true,
                ContinuationTimeout = TimeSpan.FromSeconds(config.ContinuationTimeout),
                UserName = config.UserName,
                Password = config.Password
            };

            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _replyQueueName = _channel.QueueDeclare().QueueName;
            _consumer = new EventingBasicConsumer(_channel);
            _channel.BasicConsume(_replyQueueName, true, _consumer);
        }

        public RemoteExecutorClient(IConnection connection, string executorHost)
        {
            _executorQueueName = string.Format(Constants.ChannelQueueFormat, executorHost);

            _connection = connection;
            _channel = _connection.CreateModel();
            _replyQueueName = _channel.QueueDeclare().QueueName;
            _consumer = new EventingBasicConsumer(_channel);
            _channel.BasicConsume(_replyQueueName, true, _consumer);
        }

        public Task<JobResult> RunAsync(IJob job, CancellationToken cancellationToken)
        {
            var correlationId = Guid.NewGuid().ToString();

            var props = _channel.CreateBasicProperties();
            props.ReplyTo = _replyQueueName;
            props.CorrelationId = correlationId;

            Task<JobResult> promiseTask = CreatePromiseTask(correlationId, cancellationToken);

            _channel.PublishJob(_executorQueueName, props, job);

            return promiseTask;
        }

        private Task<JobResult> CreatePromiseTask(string correlationId, CancellationToken cancellationToken)
        {
            var promise = new TaskCompletionSource<JobResult>();
            EventHandler<BasicDeliverEventArgs> recieved = null;
            recieved = (s, a) =>
            {
                if (a.BasicProperties.CorrelationId != null && a.BasicProperties.CorrelationId == correlationId)
                {
                    _consumer.Received -= recieved;
                    var result = BinarySerializer.Deserialize<JobResult>(a.Body);

                    if (result.IsFailed)
                    {
                        var message = "Job execution on {0} is failed.".LocalizeFormat(_executorHost);
                        var wrapperException = new WffExecutionException(message, result.JobException);
                        promise.TrySetException(wrapperException);
                    }
                    else if (result.IsCanceled)
                        promise.TrySetCanceled();
                    else
                        promise.TrySetResult(result);
                }
            };

            cancellationToken.Register(() => BreakPromise(correlationId));

            _consumer.Received += recieved;
            return promise.Task;
        }

        private void BreakPromise(string correlationId)
        {
            string cancelCorellationId = correlationId;

            var props = _channel.CreateBasicProperties();
            props.ReplyTo = _replyQueueName;
            props.CorrelationId = cancelCorellationId;

            _channel.PublishCancel(_executorQueueName, props);
        }

        public void Dispose()
        {
            if (_channel != null)
            {
                _channel.Close();
                _channel.Dispose();
            }

            if (_connection != null)
            {
                _connection.Close();
                _connection.Dispose();
            }
        }
    }
}
