﻿namespace Wff
{
    public class RabbitMQConfig
    {
        public string Host { get; set; } = "localhost";

        public short Port { get; set; } = 5672;

        public int ContinuationTimeout { get; set; } = 300;

        public string EntryPointExchange { get; set; } = "entry_point";

        public string UserName { get; set; } = "";

        public string Password { get; set; } = "";
    }
}
