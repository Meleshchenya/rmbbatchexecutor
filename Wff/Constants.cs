﻿namespace Wff
{
    internal static class Constants
    {
        public const string ChannelQueueFormat = "{0}_queue";

        public const string WiIdKey = "id";
        public const string WiTypeKey = "type";
        public const string WiHandlerKey = "handler";
        public const string WiArgsKey = "args";
        public const string WiWorkItemsKey = "waitFor";
        public const string WiInterruptOnFailureKey = "interruptOnFailure";

        public const string WiAttributesExecution = "execution";
    }
}
