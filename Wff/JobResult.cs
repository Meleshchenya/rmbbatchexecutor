﻿using System;

namespace Wff
{
    public enum JobExecutionStatus
    {
        None,
        Canceled,
        Completed,
        Failed
    }

    [Serializable]
    public class JobResult
    {
        public static JobResult Completed() =>
            new JobResult { Status = JobExecutionStatus.Completed };

        public static JobResult Completed(object result) =>
            new JobResult { Status = JobExecutionStatus.Completed, Body = BinarySerializer.Serialize(result) };

        public static JobResult Completed(byte[] body) =>
            new JobResult { Status = JobExecutionStatus.Completed, Body = body };

        public static JobResult Failed(Exception exception) =>
            new JobResult { Status = JobExecutionStatus.Failed, JobException = exception };

        public static JobResult Canceled() =>
            new JobResult {Status = JobExecutionStatus.Canceled};

        public JobExecutionStatus Status { get; private set; }

        public byte[] Body { get; private set; }
        
        public Exception JobException { get; set; }

        public bool HasBody => Body != null && Body.Length > 0;

        public bool IsFailed => Status == JobExecutionStatus.Failed;

        public bool IsCanceled => Status == JobExecutionStatus.Canceled;

        public TResult GetResult<TResult>()
        {
            return BinarySerializer.Deserialize<TResult>(Body);
        }        
    }
}
