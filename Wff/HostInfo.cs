﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Wff
{
    [Serializable]
    [DebuggerDisplay("{Host}")]
    public class HostInfo : IEquatable<HostInfo>
    {
        public HostInfo(string host)
        {
            Host = host;
        }

        public string Host { get; }

        public override bool Equals(object obj)
        {
            var other = obj as HostInfo;
            if (other == null)
                return false;

            return Equals(other);
        }

        public bool Equals(HostInfo other) =>
            Host.Equals(other.Host, StringComparison.OrdinalIgnoreCase);

        public override int GetHashCode() =>
            string.IsNullOrEmpty(Host) ? 0 : Host.ToUpper().GetHashCode();

        public override string ToString() => Host;
    }
}
