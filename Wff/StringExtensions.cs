﻿using System;

namespace Wff
{
    public static class StringExtensions
    {
        public static string Localize(this string str) => str;

        public static string LocalizeFormat(this string format, params object[] args) => String.Format(format, args);
    }
}
