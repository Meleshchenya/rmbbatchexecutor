﻿using System;

namespace Wff
{
    public interface IExecutionContext
    {
        Type LoadType(string typeName); 
    }
}
