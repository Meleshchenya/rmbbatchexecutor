﻿using RabbitMQ.Client;
using System;

namespace Wff
{
    internal static class ICustomModelExtensions
    {
        public static void PublishCancel(this IModel model, string routingKey, IBasicProperties basicProperties)
        {
            model.BasicPublish(String.Empty, routingKey, basicProperties, new byte[0]);
        }

        public static void PublishJob(this IModel model, string routingKey, IBasicProperties basicProperties, IJob job)
        {
            byte[] body = BinarySerializer.Serialize(job);
            model.BasicPublish(String.Empty, routingKey, basicProperties, body);
        }

        public static void PublishResult(this IModel model, string routingKey, IBasicProperties basicProperties, JobResult jobResult)
        {
            byte[] body = BinarySerializer.Serialize(jobResult);
            model.BasicPublish(String.Empty, routingKey, basicProperties, body);
        }

        public static void PublishCommand(this IModel model, string exchange, CommandRequest command)
        {
            byte[] body = BinarySerializer.Serialize(command);
            model.BasicPublish(exchange, command.Command.ToString(), null, body);
        }
    }
}
