﻿using System;
using System.Linq;
using System.Reflection;
using System.Threading;

namespace Wff
{
    [Serializable]
    public class Job : IJob
    {
        public Job(string typeName, string handler, string args = null)
        {
            TypeName = typeName;
            Handler = handler;
            Args = args;
        }

        public string TypeName { get; }
        public string Handler { get; }
        public string Args { get; set; }
        
        public JobResult Run(IExecutionContext context, CancellationToken cancellationToken)
        {
            try
            {
                string[] args = string.IsNullOrEmpty(Args) ? new string[0] : Args.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                
                Type targetType = context.LoadType(TypeName);
                MethodInfo targetMethod = GetMethod(targetType, args);
                object[] parameters = GetParameters(args, targetMethod);

                string messageEnd = string.IsNullOrEmpty(Args) ? "." : $" with args {Args}";
                NLogManager.Logger.Info($"Job {this} started" + messageEnd);

                cancellationToken.ThrowIfCancellationRequested();

                object result = null;
                Exception exception = null;
                var processFinised = new AutoResetEvent(false);
                var thread = new Thread(() =>
                {
                    try
                    {
                        result = targetMethod.Invoke(null, parameters);
                    }
                    catch (TargetInvocationException exc)
                    {
                        exception = exc.InnerException;
                    }
                    catch (Exception exc)
                    {
                        exception = exc;
                    }
                    finally
                    {
                        processFinised.Set();
                    }
                });

                cancellationToken.Register(() =>
                {
                    thread.Abort();
                    processFinised.Set();
                });

                thread.IsBackground = true;
                thread.Start();
                processFinised.WaitOne();

                if (exception != null)
                {
                    NLogManager.Logger.Error(exception);
                    return JobResult.Failed(exception);
                }

                NLogManager.Logger.Info($"Job {this} finished.");
                return result == null ? JobResult.Completed() : JobResult.Completed(result);
            }
            catch (Exception exc)
            {
                NLogManager.Logger.Error(exc, $"Job {this} failed.");
                return JobResult.Failed(exc);
            }
        }

        public override string ToString()
        {
            return string.Format($"{TypeName} - {Handler}");
        }
        
        private MethodInfo GetMethod(Type targetType, string[] args)
        {
            var argsCount = args.Length;
            var methods = targetType
                .GetMethods(BindingFlags.Static | BindingFlags.Public)
                .Where(a => a.Name == Handler)
                .ToArray();

            foreach (var methodInfo in methods)
            {
                var parameters = methodInfo.GetParameters();
                var parametersCount = parameters.Length;
                if (parametersCount == argsCount)
                    return methodInfo;

                // has params[] - currently not supported
                if (parametersCount > argsCount || parametersCount < argsCount)
                {
                    var hasParams = parameters[parameters.Length - 1].GetCustomAttributes(typeof(ParamArrayAttribute), false).Length > 0;
                    if (hasParams)
                        return methodInfo;
                }

                // has optional parameters
                if (parametersCount > argsCount)
                {
                    CheckParameterCountMismatch(parameters, argsCount);
                    return methodInfo;
                }
            }
            
            throw new MissingMethodException(TypeName, Handler);
        }

        private static object[] GetParameters(string[] args, MethodInfo mi)
        {
            var parametersInfo = mi.GetParameters();
            var argsCount = args.Length;
            var parametersCount = parametersInfo.Length;
            if (parametersCount == 0)
                return new object[0];

            var parameters = new object[parametersCount];

            for (int i = 0; i < argsCount; i++)
            {
                var arg = args[i];
                var type = parametersInfo[i].ParameterType;
                var value = Convert.ChangeType(arg, type);
                parameters[i] = value;
            }

            var remainingParameterCount = parametersCount - argsCount;
            if (remainingParameterCount > 0)
            {
                for (int i = argsCount; i <= parametersCount - remainingParameterCount; i++)
                    parameters[i] = Type.Missing;
            }

            return parameters;
        }

        private static void CheckParameterCountMismatch(ParameterInfo[] parametersInfo, int argsLength)
        {
            var optionalParametersCount = parametersInfo.Count(a => a.IsOptional);
            if (argsLength < parametersInfo.Length - optionalParametersCount)
                throw new TargetParameterCountException();
        }
    }
}