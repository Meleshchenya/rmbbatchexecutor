﻿using System.Linq;
using System.Xml.Linq;
using Wff.Xml;

namespace Wff
{
    public class VariableDescription : ElementDescription
    {
        private const string IdKey = "key";
        private const string HostKey = "value";

        public string Key { get; set; }
        public string Value { get; set; }

        public override void Attach(XElement jobElement)
        {
            if (!TrySetElement<VariableDescription>(jobElement, IdKey, (j, v) => j.Key = v)
                || !TrySetElement<VariableDescription>(jobElement, HostKey, (j, v) => j.Value = v))
            {
                int position = jobElement.NodesBeforeSelf().Count();
                throw new WffException("Variable element at position {0} has wrong format.".LocalizeFormat(position));
            }
        }
    }
}