﻿using System.Linq;
using System.Xml.Linq;
using Wff.Xml;

namespace Wff
{
    public class ExecutorDescription : ElementDescription
    {
        private const string IdKey = "id";
        private const string HostKey = "host";

        public string Id { get; private set; }
        public string Host { get; private set; }

        public override void Attach(XElement jobElemnt)
        {
            if (!TrySetElement<ExecutorDescription>(jobElemnt, IdKey, (j, v) => j.Id = v)
                || !TrySetElement<ExecutorDescription>(jobElemnt, HostKey, (j, v) => j.Host = v))
            {
                int position = jobElemnt.NodesBeforeSelf().Count();
                throw new WffException("Executor element at position {0} has worng format.".LocalizeFormat(position));
            }
        }
    }
}
