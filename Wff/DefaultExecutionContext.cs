﻿using System;
using System.IO;
using System.Reflection;

namespace Wff
{
    public class DefaultExecutionContext : IExecutionContext
    {
        private readonly string _pluginLocation;

        public DefaultExecutionContext(string pluginLocation)
        {
            _pluginLocation = pluginLocation;
        }

        public Type LoadType(string typeName)
        {
            return Type.GetType(typeName, LoadAssembly, LoadType);
        }

        private Assembly LoadAssembly(AssemblyName assemblyName)
        {
            string fileName = assemblyName.Name + ".dll";
            string pluginFolderPath = Path.GetFullPath(_pluginLocation);

            NLogManager.Logger.Info($"Loading {assemblyName.Name} from {pluginFolderPath}");

            string assemblyPath = Path.Combine(pluginFolderPath, fileName);
            if (File.Exists(assemblyPath))
                return Assembly.LoadFrom(assemblyPath);

            throw new FileNotFoundException(String.Empty, assemblyPath);
        }

        private static Type LoadType(Assembly assembly, string typeName, bool ignoreCase)
        {
            return assembly.GetType(typeName, true, ignoreCase);
        }
    }
}
