﻿using System;

namespace Wff
{
    public class SubscriptionBinder<TPayload> : ICommandSubscriber
    {
        private readonly Action<TPayload> _publishAction;
        private readonly Func<CommandRequest, TPayload> _selector;

        public SubscriptionBinder(Action<TPayload> publishAction, Func<CommandRequest, TPayload> selector)
        {
            _publishAction = publishAction;
            _selector = selector;
        }

        public void PublishCommand(CommandRequest commandRequest)
        {
            TPayload payload = _selector(commandRequest);
            _publishAction(payload);
        }
    }
}
