﻿using System.Collections.Generic;

namespace Wff
{
    public interface IScheduler<TExecutorKey>
    {
        void AddExecutor(TExecutorKey executroKey, IJobExecutor executor);
        void RemoveExecutor(TExecutorKey executroKey);
        IJobExecutor GetExecutor(TExecutorKey executorKey);
        bool ExecuteWorkflow(IEnumerable<IWorkItem<TExecutorKey>> workflow);
        void InterruptExecution();
    }
}
