﻿using System;
using System.Collections.Generic;

namespace Wff
{
    static class IEnumerableExtensions
    {
        public static void Iter<T>(this IEnumerable<T> enumerable, Action<T> prdicate)
        {
            foreach (var item in enumerable)
                prdicate(item);
        }
    }
}
