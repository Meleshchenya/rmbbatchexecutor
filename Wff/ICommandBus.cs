﻿using System;

namespace Wff
{
    public interface ICommandBus
    {
        IDisposable Subscribe(ControlCommand command, ICommandSubscriber subscriber);
        void Publish(CommandRequest commandRequest);
    }

    public interface ICommandSubscriber
    {
        void PublishCommand(CommandRequest commandRequest);
    }
}
