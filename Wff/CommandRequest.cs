﻿using System;

namespace Wff
{
    public enum ControlCommand
    {
        None,
        AddHost,
        RemoveHost,
        NotifyMasterStarted,
        Ping,
        ACK
    }

    [Serializable]
    public class CommandRequest
    {
        public CommandRequest(string host, ControlCommand command) : this(new HostInfo(host), command) { }

        public CommandRequest(HostInfo host, ControlCommand command)
        {
            InitiatorHost = host;
            Command = command;
        }

        public HostInfo InitiatorHost { get; private set; }
        public ControlCommand Command { get; private set; }
    }

    public static class ControlCommandExtensions
    {
        public static void Publish(this ControlCommand command, ICommandBus commandBus, string publisherHostName)
        {
            commandBus.Publish(new CommandRequest(publisherHostName, command));
        }

        public static void Publish(this ControlCommand command, ICommandBus commandBus, HostInfo publisherHost)
        {
            commandBus.Publish(new CommandRequest(publisherHost, command));
        }
    }
}
