﻿using System.Threading;

namespace Wff
{
    public interface IJob
    {
        JobResult Run(IExecutionContext context, CancellationToken cancellationToken);
    }
}
