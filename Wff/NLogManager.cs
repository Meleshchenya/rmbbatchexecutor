﻿using NLog;
using NLog.Config;
using System.Configuration;

namespace Wff
{
    public static class NLogManager
    {
        const string LoggerName = "BatchExecutor";

        static NLogManager()
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            LogManager.Configuration = new XmlLoggingConfiguration(config.FilePath);

            Logger = LogManager.GetLogger(LoggerName);
        }

        public static Logger Logger { get; }
    }
}