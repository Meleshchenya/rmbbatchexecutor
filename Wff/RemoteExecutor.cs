﻿using System;
using RabbitMQ.Client;
using System.Threading;
using RabbitMQ.Client.Events;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Wff
{
    public class RemoteExecutor : ChannelServer
    {
        private readonly NLog.Logger _logger = NLogManager.Logger;
        private readonly IJobExecutor _executor;

        private readonly Dictionary<string, CancellationTokenSource> _cancellations =
            new Dictionary<string, CancellationTokenSource>();

        private readonly object _sync = new object();

        public RemoteExecutor(IJobExecutor executor, string executorHost)
            : base(string.Format(Constants.ChannelQueueFormat, executorHost))
        {
            _executor = executor;
        }

        public RemoteExecutor(IJobExecutor executor, IConnection connection, string executorHost)
            : base(connection, string.Format(Constants.ChannelQueueFormat, executorHost))
        {
            _executor = executor;
        }

        protected override async void MessageReceived(object sender, BasicDeliverEventArgs args)
        {
            var requestProperties = args.BasicProperties;
            var replyProperties = Channel.CreateBasicProperties();
            replyProperties.CorrelationId = requestProperties.CorrelationId;

            CancellationTokenSource cancellation;
            lock (_sync)
            {
                if (!_cancellations.TryGetValue(replyProperties.CorrelationId, out cancellation))
                    _cancellations[replyProperties.CorrelationId] = cancellation = new CancellationTokenSource();
            }

            if (args.Body != null && args.Body.Length > 0)
                await RunJob(args, requestProperties, replyProperties, cancellation.Token);
            else
                cancellation.Cancel();
        }

        private async Task RunJob(
            BasicDeliverEventArgs args, 
            IBasicProperties requestProperties,
            IBasicProperties replyProperties,
            CancellationToken cancellationToken)
        {
            JobResult response = null;
            IJob job = null;
            try
            {
                job = BinarySerializer.Deserialize<IJob>(args.Body);
                _logger.Info($"Job '{job}' received.");

                if (!cancellationToken.IsCancellationRequested)
                    response = await _executor.RunAsync(job, cancellationToken);
                else
                    response = JobResult.Canceled();

                _logger.Info($"Job execution of '{job}' finished with status {response.Status}.");
            }
            catch (Exception exc)
            {
                response = JobResult.Failed(exc);
                string id = job != null ? job.ToString() : "unserialized";
                _logger.Error(exc, $"Job excution of '{id}' failed.");
            }
            finally
            {
                string replyQueueName = requestProperties.ReplyTo;
                Channel.PublishResult(replyQueueName, replyProperties, response);
                lock (_sync)
                {
                    if (_cancellations.ContainsKey(replyProperties.CorrelationId))
                        _cancellations.Remove(replyProperties.CorrelationId);
                }
            }
        }

        public override void Start()
        {
            base.Start();
            OnExecutorStarted();
        }

        public override void Stop()
        {
            OnExecutorStopped();
            base.Stop();
        }

        public event Action ExecutorStarted;
        public event Action ExecutorStopped;

        private void OnExecutorStarted()
        {
            ExecutorStarted?.Invoke();
        }

        private void OnExecutorStopped()
        {
            ExecutorStopped?.Invoke();
        }
    }
}
