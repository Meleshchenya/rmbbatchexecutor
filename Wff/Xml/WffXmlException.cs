﻿using System;
using System.Runtime.Serialization;

namespace Wff.Xml
{
    [Serializable]
    public class WffXmlException : WffException
    {
        public WffXmlException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public WffXmlException(string message) : base(message)
        {
        }

        public WffXmlException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}
