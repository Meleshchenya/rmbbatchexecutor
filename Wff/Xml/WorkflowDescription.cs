﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace Wff.Xml
{
    public class WorkflowDescription
    {
        private List<ExecutorDescription> _executors;
        private List<VariableDescription> _variables;
        private List<WorkItemDescription> _workItems;
        private Dictionary<string, string> _arguments;

        public List<ExecutorDescription> Executors => _executors ?? (_executors = new List<ExecutorDescription>());
        public List<VariableDescription> Variables => _variables ?? (_variables = new List<VariableDescription>());
        public List<WorkItemDescription> WorkItems => _workItems ?? (_workItems = new List<WorkItemDescription>());
        
        public void Load(string filePath, Dictionary<string, string> arguments = null)
        {
            _arguments = arguments != null
                ? new Dictionary<string, string>(arguments)
                : new Dictionary<string, string>();

            try
            {
                var root = XElement.Load(filePath);

                _executors = Read<ExecutorDescription>(root, "Executors", true);
                _workItems = Read<WorkItemDescription>(root, "WorkItems", true);
                _variables = Read<VariableDescription>(root, "Variables");

                BuildWorkflow();
            }
            catch (Exception exc)
            {
                throw new WffXmlException("Workflow configuration loading error.".Localize(), exc);
            }
        }

        private static List<T> Read<T>(XElement root, string elementName, bool required = false) where T : ElementDescription, new()
        {
            var result = new List<T>();

            var element = root.Element(XName.Get(elementName));
            if (required && element == null)
                throw new WffXmlException($"Required element '{elementName}' is missing.".Localize());

            if (required && element.IsEmpty)
                throw new WffXmlException($"'{elementName}' collection is empty.".Localize());

            if (element != null && !element.IsEmpty)
            {
                var elements = element.Elements();

                foreach (XElement executorElement in elements)
                {
                    var obj = new T();
                    obj.Attach(executorElement);
                    result.Add(obj);
                }
            }

            return result;
        }

        private void BuildWorkflow()
        {
            if (_variables.Count == 0 && _arguments.Count == 0)
                return;

            var variables = _variables.ToDictionary(a => $"{{{a.Key}}}", b => b.Value, StringComparer.OrdinalIgnoreCase);
            var arguments = _arguments.ToDictionary(a => $"@{a.Key}", b => b.Value, StringComparer.OrdinalIgnoreCase);
            
            var variablesRegex = new Regex("(?<arg>{[(a-zA-Z0-9)]+})");
            var argumentsRegex = new Regex("(?<arg>@[(a-zA-Z0-9)]+)([,; |]|$)");

            variables = UpdateVariables(variables, arguments, argumentsRegex);
            variables = UpdateVariables(variables, variables, variablesRegex);
            
            foreach (var workItem in WorkItems)
            {
                Update(workItem, variables, variablesRegex);
                Update(workItem, arguments, argumentsRegex);
            }
            
            void Update(WorkItemDescription workItem, Dictionary<string, string> values, Regex regex)
            {
                Func<string, string> replace = s => Replace(s, values, regex);

                if (workItem.Id != null) workItem.Id = replace(workItem.Id);
                if (workItem.Args != null) workItem.Args = replace(workItem.Args);
                if (workItem.Handler != null) workItem.Handler = replace(workItem.Handler);
                if (workItem.TargetType != null) workItem.TargetType = replace(workItem.TargetType);
                if (workItem.PendingWorkItems != null) workItem.PendingWorkItems = replace(workItem.PendingWorkItems);
                if (workItem.ExecutionAttributes != null)
                {
                    var attributes = new Dictionary<string, string>();

                    foreach (var attribute in workItem.ExecutionAttributes)
                    {
                        var attrKey = attribute.Key;
                        var attrValue = replace(attribute.Value);

                        attributes.Add(attrKey, attrValue);
                    }

                    foreach (var attribute in attributes)
                        workItem.ExecutionAttributes[attribute.Key] = attribute.Value;
                }
            }
        }

        private static Dictionary<string, string> UpdateVariables(
            Dictionary<string, string> replaceWhat,
            Dictionary<string, string> replaceWith,
            Regex variablesRegex)
        {
            var result = new Dictionary<string, string>();

            foreach (var kvp in replaceWhat)
            {
                var newValue = Replace(kvp.Value, replaceWith, variablesRegex);

                result.Add(kvp.Key, newValue);
            }

            return result;
        }

        private static string Replace(string s, Dictionary<string, string> values, Regex regex)
        {
            var matchCollection = regex.Matches(s);
            if (matchCollection.Count > 0)
            {
                foreach (Match match in matchCollection)
                {
                    var groupValue = match.Groups["arg"].Value;
                    s = s.Replace(groupValue, values.TryGetValue(groupValue, out var value) ? value : "");
                }
            }

            return s;
        }
    }
}
