﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Wff.Xml
{
    public class WorkItemBuildState
    {
        public Dictionary<string, HostInfo> ExecutorHosts { get; set; }
        public Dictionary<string, List<Task<JobResult>>> AsyncWorkItemTasks { get; set; }
    }
}