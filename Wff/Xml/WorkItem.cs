﻿using System;
using NLog;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Wff.Xml
{
    public class WorkItem : IWorkItem<HostInfo>
    {
        private readonly Logger _logger = NLogManager.Logger;
        private readonly bool _interruptFailed;

        public WorkItem(WorkItemBuildState buildState, WorkItemDescription description)
        {
            AssignedHosts =  GetHosts(buildState, description);
            TargetJob = new Job(description.TargetType, description.Handler, description.Args);
            Id = description.Id;
            _interruptFailed = description.InterruptOnFailure;
        }

        public WorkItem(string id, Job job, List<HostInfo> hosts)
        {
            AssignedHosts = hosts;
            TargetJob = job;
            Id = id;
        }

        public List<HostInfo> AssignedHosts { get; }
        public Job TargetJob { get; set; }
        public string Id { get; set; }
        
        public virtual async Task<bool> DoWorkAsync(IScheduler<HostInfo> scheduler, CancellationToken cancellationToken)
        {
            _logger.Info($"WorkItem '{Id}' started.");

            bool canContinue = true;
            var executors = AssignedHosts.Select(scheduler.GetExecutor);
            try
            {
                await Task.Run(() => Parallel.ForEach(executors, e => e.RunAsync(TargetJob, cancellationToken).Wait(cancellationToken)), cancellationToken);
                _logger.Info($"WorkItem '{Id}' finished.");
            }
            catch (TaskCanceledException)
            {
                canContinue = false;
                _logger.Warn($"WorkItem '{Id}' canceled.");
            }
            catch (WffExecutionException exc)
            {
                canContinue = !_interruptFailed;
                _logger.Error($"WorkItem '{Id}' failed.");
                _logger.Error(exc);
            }
            catch (AggregateException exc)
            {
                canContinue = !_interruptFailed;
                _logger.Error($"WorkItem '{Id}' failed.");
                exc.Flatten().InnerExceptions.Iter(_logger.Error);
            }

            return canContinue;
        }

        private List<HostInfo> GetHosts(WorkItemBuildState buildState, WorkItemDescription description)
        {
            string hostString = description.ExecutionAttributes[Constants.WiAttributesExecution];
            string[] hosts = hostString.Split(',');

            var assignedHosts = new List<HostInfo>();
            foreach (var host in hosts)
            {
                HostInfo hostInfo;
                if (!buildState.ExecutorHosts.TryGetValue(host, out hostInfo))
                    throw new WffException("Executor {0} not found.".LocalizeFormat(host));

                assignedHosts.Add(hostInfo);
            }

            return assignedHosts;
        }
    }
}
