﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Wff.Xml
{
    public class HostDependentWorkflow : IEnumerable<IWorkItem<HostInfo>>
    {
        private Lazy<IEnumerator<IWorkItem<HostInfo>>> _workItemEnumerator;

        public HostDependentWorkflow(WorkflowDescription description)
        {
            _workItemEnumerator = new Lazy<IEnumerator<IWorkItem<HostInfo>>>(() => new WorkItemEnumerator(description));
        }

        public HostDependentWorkflow(IEnumerable<IWorkItem<HostInfo>> workItems)
        {
            _workItemEnumerator = new Lazy<IEnumerator<IWorkItem<HostInfo>>>(() => workItems.GetEnumerator());
        }

        public IEnumerator<IWorkItem<HostInfo>> GetEnumerator()
        {
            return _workItemEnumerator.Value;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _workItemEnumerator.Value;
        }

        private class WorkItemEnumerator : IEnumerator<IWorkItem<HostInfo>>, IEnumerator
        {
            private readonly IEnumerator<WorkItemDescription> _workItemEnumerator;
            private readonly WorkItemBuildState _buildState;
            private IWorkItem<HostInfo> _current;

            public WorkItemEnumerator(WorkflowDescription description)
            {
                _workItemEnumerator = description.WorkItems.GetEnumerator();
                _buildState = new WorkItemBuildState
                {
                    ExecutorHosts = description.Executors.ToDictionary(_ => _.Id, _ => new HostInfo(_.Host)),
                    AsyncWorkItemTasks = new Dictionary<string, List<Task<JobResult>>>(),
                };
            }

            public IWorkItem<HostInfo> Current => _current ?? (_current = GetCurrent());

            object IEnumerator.Current => Current;

            public void Dispose()
            {
                _workItemEnumerator.Dispose();
            }

            public bool MoveNext()
            {
                _current = null;
                return _workItemEnumerator.MoveNext();
            }

            public void Reset()
            {
                _current = null;
                _workItemEnumerator.Reset();
            }

            private IWorkItem<HostInfo> GetCurrent()
            {
                WorkItemDescription current = _workItemEnumerator.Current;
                switch (current.WorkItemType)
                {
                    case WorkItemRunType.Run:
                        return new WorkItem(_buildState, current);
                    case WorkItemRunType.RunAsync:
                        return new AsyncWorkItem(_buildState, current);
                    case WorkItemRunType.Barrier:
                        return new Barrier(_buildState, current);
                    default:
                        throw new WffXmlException("WorkItem {0} has unsupported type {1}.".LocalizeFormat(current.Id, current.WorkItemType));
                }
            }
        }
    }
}
