﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Xml.Linq;

namespace Wff.Xml
{
    public enum WorkItemRunType
    {
        Run,
        RunAsync,
        Barrier
    }

    [DebuggerDisplay("{Id},{TargetType}.{Handler}")]
    public class WorkItemDescription : ElementDescription
    {
        private WorkItemRunType _workItemType;
        private Dictionary<string, string> _attributes;
        private readonly HashSet<string> _knownAttributes =
            new HashSet<string>(new[] 
            { Constants.WiIdKey,
                Constants.WiTypeKey,
                Constants.WiHandlerKey,
                Constants.WiArgsKey,
                Constants.WiWorkItemsKey,
                Constants.WiInterruptOnFailureKey
            });

        public string Id { get; set; }

        public string TargetType { get; set; }

        public string Handler { get; set; }

        public string Args { get; set; }

        public string PendingWorkItems { get; set; }

        public bool InterruptOnFailure { get; set; }

        public WorkItemRunType WorkItemType { get { return _workItemType; } set { _workItemType = value; } }

        public Dictionary<string, string> ExecutionAttributes => 
            _attributes ?? (_attributes = new Dictionary<string, string>());

        public override void Attach(XElement workItemElement)
        {
            bool isCorrect = false;

            if (Enum.TryParse(workItemElement.Name.LocalName, true, out _workItemType))
            {
                switch (_workItemType)
                {
                    case WorkItemRunType.Run:
                    case WorkItemRunType.RunAsync:
                        isCorrect = TryAttachRun(workItemElement);
                        break;
                    case WorkItemRunType.Barrier:
                        isCorrect = TryAttachBarrier(workItemElement);
                        break;
                }
            }

            if (!isCorrect)
            {
                int position = workItemElement.NodesBeforeSelf().Count();
                throw new WffXmlException("WorkItems element at position {0} has wrong format.".LocalizeFormat(position));
            }

            AttachKnownAttributes(workItemElement);
        }

        private bool TryAttachBarrier(XElement element)
        {
            bool iterrupt;
            TrySetElement<WorkItemDescription>(element, Constants.WiInterruptOnFailureKey, (j, v) => j.InterruptOnFailure = !bool.TryParse(v, out iterrupt) || iterrupt);

            return TrySetElement<WorkItemDescription>(element, Constants.WiWorkItemsKey, (j, v) => j.PendingWorkItems = v);
        }

        private bool TryAttachRun(XElement element)
        {
            TrySetElement<WorkItemDescription>(element, Constants.WiInterruptOnFailureKey,
                (j, v) => j.InterruptOnFailure = !bool.TryParse(v, out bool iterrupt) || iterrupt);
            TrySetElement<WorkItemDescription>(element, Constants.WiArgsKey, (j, v) => j.Args = v);

            return TrySetElement<WorkItemDescription>(element, Constants.WiIdKey, (j, v) => j.Id = v)
                   && TrySetElement<WorkItemDescription>(element, Constants.WiTypeKey, (j, v) => j.TargetType = v)
                   && TrySetElement<WorkItemDescription>(element, Constants.WiHandlerKey, (j, v) => j.Handler = v);
        }

        private void AttachKnownAttributes(XElement element)
        {
            IEnumerable<XAttribute> enumerable = element.Attributes();
            foreach (var attribute in enumerable)
            {
                string name = attribute.Name.LocalName;
                if (!_knownAttributes.Contains(name))
                    ExecutionAttributes[name] = attribute.Value;
            }
        }
    }
}
