﻿using System;
using System.Xml.Linq;

namespace Wff.Xml
{
    public abstract class ElementDescription
    {
        public abstract void Attach(XElement xElement);

        protected bool TrySetElement<T>(XElement element, string key, Action<T, string> set) where T : ElementDescription
        {
            XAttribute attribute = element.Attribute(XName.Get(key));
            if (attribute == null)
                return false;

            set((T) this, attribute.Value);
            return true;
        }
    }
}
