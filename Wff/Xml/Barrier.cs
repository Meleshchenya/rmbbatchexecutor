﻿using NLog;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Wff.Xml
{
    public class Barrier : IWorkItem<HostInfo>
    {
        private readonly Logger _logger = NLogManager.Logger;
        private readonly WorkItemBuildState _buildState;
        private readonly string[] _pendingWorkItems;
        private readonly bool _interruptFailed;

        public Barrier(WorkItemBuildState buildState, WorkItemDescription description)
        {
            _buildState = buildState;
            _pendingWorkItems = description.PendingWorkItems.Split(',');
            _interruptFailed = description.InterruptOnFailure;
        }

        public async Task<bool> DoWorkAsync(IScheduler<HostInfo> scheduler, CancellationToken cancellationToken)
        {
            var barrierId = Guid.NewGuid();
            _logger.Info($"Barrier '{barrierId}' started. Waiting for '{string.Join(",", _pendingWorkItems)}'");

            var pendingWorkItemTasks = new List<Task<JobResult>>();
            foreach (var workItem in _pendingWorkItems)
            {
                if (!_buildState.AsyncWorkItemTasks.TryGetValue(workItem, out var pendingTasks))
                    throw new WffExecutionException("Pending WorkItem {0} is not started.".LocalizeFormat(workItem));

                pendingWorkItemTasks.AddRange(pendingTasks);
            }

            bool canContinue = true;
            Task waitTask = null;
            try
            {
                waitTask = Task.WhenAll(pendingWorkItemTasks);
                await waitTask;
                _logger.Info($"Barrier '{barrierId}' finished.");
            }
            catch (TaskCanceledException)
            {
                canContinue = false;
                _logger.Warn($"Barrier '{barrierId}' canceled.");
            }
            catch (WffExecutionException)
            {
                canContinue = !_interruptFailed;
                _logger.Error($"Barrier '{barrierId}' failed.");

                if (waitTask?.Exception != null)
                    foreach (var innerException in waitTask.Exception.InnerExceptions)
                        _logger.Error(innerException);
            }
            catch (AggregateException exc)
            {
                canContinue = !_interruptFailed;
                _logger.Error($"Barrier '{barrierId}' failed.");
                exc.Flatten().InnerExceptions.Iter(_logger.Error);
            }

            return canContinue;
        }
    }
}
