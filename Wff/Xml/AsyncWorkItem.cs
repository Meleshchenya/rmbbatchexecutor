﻿using NLog;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Wff.Xml
{
    public class AsyncWorkItem : WorkItem
    {
        private readonly Logger _logger = NLogManager.Logger;
        private readonly WorkItemBuildState _buildState;

        public AsyncWorkItem(WorkItemBuildState buildState, WorkItemDescription description) 
            : base(buildState, description)
        {
            _buildState = buildState;
        }
        
        public override Task<bool> DoWorkAsync(IScheduler<HostInfo> scheduler, CancellationToken cancellationToken)
        {
            _logger.Info($"Async work item '{Id}' started.");

            var tasks = new List<Task<JobResult>>(AssignedHosts.Count);
            var calcelationSource = new CancellationTokenSource();
            cancellationToken.Register(calcelationSource.Cancel);

            foreach (var host in AssignedHosts)
            {
                var executor = scheduler.GetExecutor(host);
                var task = executor.RunAsync(TargetJob, calcelationSource.Token);
                tasks.Add(task);
            }
            _buildState.AsyncWorkItemTasks[Id] = tasks;

            return Task.FromResult(true);
        }
    }
}