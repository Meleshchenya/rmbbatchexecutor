﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Wff
{
    public class AtlExecutor : IJobExecutor
    {
        private readonly IExecutionContext _context;

        public AtlExecutor(IExecutionContext context)
        {
            _context = context;
        }

        public async Task<JobResult> RunAsync(IJob job, CancellationToken cancellationToken)
        {
            JobResult result = await Task.Factory.StartNew(() => job.Run(_context, cancellationToken), cancellationToken);

            if (cancellationToken.IsCancellationRequested)
                return JobResult.Canceled();

            return result;
        }
    }
}
