﻿using System;
using System.IO;
using System.Reflection;

namespace Wff
{
    public static class PathResolver
    {
        public static string GetPath(string subPath)
        {
            string codeBase = Assembly.GetExecutingAssembly().CodeBase;
            UriBuilder uri = new UriBuilder(codeBase);
            string path = Uri.UnescapeDataString(uri.Path);
            string directoryName = Path.GetDirectoryName(path);

            return Path.Combine(directoryName, subPath);
        }
    }
}
