﻿using System;
using System.Collections.Generic;
using System.Threading;
using NLog;

namespace Wff
{
    public class RemoteScheduler : IScheduler<HostInfo>
    {
        private readonly Dictionary<HostInfo, IJobExecutor> _executors = new Dictionary<HostInfo, IJobExecutor>();
        private readonly CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();
        private readonly Logger _logger = NLogManager.Logger;

        public void AddExecutor(HostInfo executorKey, IJobExecutor executor) => _executors.Add(executorKey, executor);

        public void RemoveExecutor(HostInfo executroKey) => _executors.Remove(executroKey);

        public bool ExecuteWorkflow(IEnumerable<IWorkItem<HostInfo>> workflow)
        {
            foreach (var workItem in workflow)
            {
                try
                {
                    bool canContinue = workItem.DoWorkAsync(this, _cancellationTokenSource.Token).Result;
                    if (!canContinue)
                        return false;
                }
                catch (Exception exc)
                {
                    _logger.Error(exc);
                    return false;
                }
            }

            return true;
        }

        public void InterruptExecution()
        {
            _cancellationTokenSource.Cancel();
        }

        public IJobExecutor GetExecutor(HostInfo executorKey)
        {
            IJobExecutor executor;

            if (!_executors.TryGetValue(executorKey, out executor))
                throw new WffException("Job executor for {0} is not found.".LocalizeFormat(executorKey));

            return executor;
        }
    }
}
