﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;

namespace Wff
{
    public class CommandBus : ICommandBus, IDisposable
    {
        private readonly NLog.Logger _logger = NLogManager.Logger;

        private readonly Dictionary<ControlCommand, List<ICommandSubscriber>> _subscribers = 
            new Dictionary<ControlCommand, List<ICommandSubscriber>>();

        private readonly Lazy<IModel> _channel;
        private IConnection _connection;
        private string _queueName;
        private EventingBasicConsumer _consumer;
        private readonly RabbitMQConfig _config;

        public CommandBus()
        {
            var configProvider = new ConfigProvider<RabbitMQConfig>();
            _config = configProvider.GetConfig("config.json");

            _channel = new Lazy<IModel>(InitializeChannel);
        }

        public IDisposable Subscribe(ControlCommand command, ICommandSubscriber subscriber)
        {
            List<ICommandSubscriber> commandSubscribers;
            if (!_subscribers.TryGetValue(command, out commandSubscribers))
            {
                commandSubscribers = new List<ICommandSubscriber>();
                _subscribers[command] = commandSubscribers;
                _channel.Value.QueueBind(_queueName, _config.EntryPointExchange, command.ToString());
            }

            commandSubscribers.Add(subscriber);

            return new Unsubscriber(this, command, subscriber);
        }

        public void Publish(CommandRequest commandRequest)
        {
            _channel.Value.PublishCommand(_config.EntryPointExchange, commandRequest);
        }

        public void Dispose()
        {
            if (_channel.IsValueCreated)
            {
                _channel.Value?.Close();
                _channel.Value?.Dispose();
            }

            if (_connection != null)
            {
                _connection.Close();
                _connection.Dispose();
            }

            if (_consumer != null)
                _consumer.Received -= MessageReceived;
        }

        private IModel InitializeChannel()
        {
            var factory = new ConnectionFactory
            {
                HostName = _config.Host,
                Port = _config.Port,
                AutomaticRecoveryEnabled = true,
                ContinuationTimeout = TimeSpan.FromSeconds(_config.ContinuationTimeout),
                UserName = _config.UserName,
                Password = _config.Password
            };

            _connection = factory.CreateConnection();
            var channel = _connection.CreateModel();
            channel.ExchangeDeclare(_config.EntryPointExchange, "direct", autoDelete: true);

            _queueName = channel.QueueDeclare().QueueName;

            _consumer = new EventingBasicConsumer(channel);
            _consumer.Received += MessageReceived;
            channel.BasicConsume(_queueName, true, _consumer);

            return channel;
        }

        private void MessageReceived(object sender, BasicDeliverEventArgs args)
        {
            _logger.Info($"Command message received.");

            var commandRequest = BinarySerializer.Deserialize<CommandRequest>(args.Body);

            List<ICommandSubscriber> commandSubscribers;
            if (_subscribers.TryGetValue(commandRequest.Command, out commandSubscribers))
            {
                foreach (var subscriber in commandSubscribers)
                    subscriber.PublishCommand(commandRequest);

                _logger.Info($"Command message '{commandRequest.Command}' from '{commandRequest.InitiatorHost}' processed.");
            }
        }

        private class Unsubscriber : IDisposable
        {
            private readonly ICommandSubscriber _subscriber;
            private readonly ControlCommand _command;
            readonly CommandBus _bus;

            public Unsubscriber(CommandBus bus, ControlCommand command, ICommandSubscriber subscriber)
            {
                _subscriber = subscriber;
                _command = command;
                _bus = bus;
            }

            public void Dispose()
            {
                List<ICommandSubscriber> commandSubscribers;
                if (_bus._subscribers.TryGetValue(_command, out commandSubscribers))
                    commandSubscribers.Remove(_subscriber);
            }
        }
    }
}
