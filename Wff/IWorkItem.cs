﻿using System.Threading;
using System.Threading.Tasks;

namespace Wff
{
    public interface IWorkItem<TKey>
    {
        Task<bool> DoWorkAsync(IScheduler<TKey> scheduler, CancellationToken cancellationToken);
    }
}
