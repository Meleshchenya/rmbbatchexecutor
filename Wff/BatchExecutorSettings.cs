﻿using System.Configuration;

namespace Wff
{
    public sealed class BatchExecutorSettings : ConfigurationSection
    {
        private static volatile BatchExecutorSettings _instance;
        private static readonly object SyncRoot = new object();

        public static BatchExecutorSettings Default
        {
            get
            {
                if (_instance == null)
                {
                    lock (SyncRoot)
                    {
                        if (_instance == null)
                            _instance = (BatchExecutorSettings)ConfigurationManager.GetSection(nameof(BatchExecutorSettings));
                        if (_instance == null)
                            _instance = new BatchExecutorSettings();
                    }
                }

                return _instance;
            }
        }

        [ConfigurationProperty("pluginLocation", DefaultValue = "Plugins")]
        public string PluginLocation
        {
            get => (string)base["pluginLocation"];
            set => base["pluginLocation"] = value;
        }
    }
}
