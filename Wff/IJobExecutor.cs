﻿using System.Threading;
using System.Threading.Tasks;

namespace Wff
{
    public interface IJobExecutor
    {
        Task<JobResult> RunAsync(IJob job, CancellationToken cancellationToken);
    }
}
